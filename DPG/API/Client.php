<?php

namespace DPG\API;

class Client {
	/**
	 * @var string
	 */
	private $endpoint = 'https://platform.digitalpropertygroup.com/api/v1/';

	/**
	 * @var string
	 */
	private $token = null;


	private $guzzleClient = null;

	public function __construct( $token = null ) {

		$this->token = $token;

		$this->guzzleClient = new \GuzzleHttp\Client( [
			'base_uri' => $this->endpoint,
		] );
	}

	/**
	 * @method
	 * @description Add the token on every request
	 *
	 * @param $method
	 * @param string $uri
	 * @param array $options
	 *
	 * @return mixed|\Psr\Http\Message\ResponseInterface
	 */
	public function request( $method, $uri = '', array $options = [] ) {
		if ( isset( $options['query'] ) && $options['query'] && ! isset( $options['query']['api_token'] ) ) {
			$options['query']['api_token'] = $this->token;
		}

		return $this->guzzleClient->request( $method, $uri, $options );
	}
}
