<?php

namespace DPG\API;

class API {
	/**
	 * @var Client
	 */
	private $client;

	/**
	 * API constructor.
	 *
	 * @param $token
	 */
	public function __construct( $token ) {
		$this->client = new Client( $token );
	}

	/**
	 * @param $query
	 *
	 * @return array|mixed|object
	 */
	public function getProperties( $query ) {
		$curl_response = $this->client->request( 'GET', 'properties', [
			'query' => $query
		] );

		return $this->getResponse( $curl_response );
	}

	/**
	 * @param $query
	 *
	 * @return array|mixed|object
	 */
	public function getBranches( $query ) {
		$curl_response = $this->client->request( 'GET', 'branches', [
			'query' => $query
		] );

		return $this->getResponse( $curl_response );
	}

	/**
	 * @param $query
	 *
	 * @return array|mixed|object
	 */
	public function getSuburbDemographics( $query ) {
		$curl_response = $this->client->request( 'GET', 'suburb-demographics', [
			'query' => $query
		] );

		return $this->getResponse( $curl_response );
	}

	private function getResponse( $curl_response ) {
		$response = json_decode( $curl_response->getBody()->getContents() );

		return $response->data;
	}
}