<?php

namespace DPG\WP\Contracts;

interface DataStore {
	/**
	 * @return mixed
	 */
	public function get();

	/**
	 * Returns single field
	 *
	 * @param $field
	 *
	 * @return mixed|null
	 */
	public function getField( $field );

	/**
	 * @param $data
	 *
	 * @return void
	 */
	public function update( $data );

	/**
	 * Updates acf field
	 *
	 * @param $field
	 * @param $value
	 */
	public function updateField( $field, $value );
}