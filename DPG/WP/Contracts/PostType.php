<?php

namespace DPG\WP\Contracts;

interface PostType {
	static function registerPostType();
	static function registerDataStore();
}