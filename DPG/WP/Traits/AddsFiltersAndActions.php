<?php

namespace DPG\WP\Traits;

/**
 * Trait AddsFiltersAndActions
 * @package DPG\WP\Traits
 */
trait AddsFiltersAndActions {

	/**
	 * Adds action, giving the object as a reference
	 *
	 * @param $hook
	 * @param $callback
	 * @param int $priority
	 * @param int $accepted_args
	 */
	public function add_action( $hook, $callback, $priority = 10, $accepted_args = 1 ) {
		add_action( $hook, array( $this, $callback ), $priority, $accepted_args );
	}


	/**
	 * Adds filter, giving the object as a reference
	 *
	 * @param $hook
	 * @param $callback
	 * @param int $priority
	 * @param int $accepted_args
	 */
	public function add_filter( $hook, $callback, $priority = 10, $accepted_args = 1 ) {
		add_filter( $hook, array( $this, $callback ), $priority, $accepted_args );
	}
}