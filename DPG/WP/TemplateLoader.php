<?php

namespace DPG\WP;

class TemplateLoader {
	/**
	 * Hook in methods.
	 */
	public static function init() {
		add_filter( 'template_include', array( __CLASS__, 'template_loader' ) );
	}

	/**
	 * @param $template
	 *
	 * @return mixed
	 */
	static function template_loader( $template ) {
		$file = '';
		if ( is_singular( 'property' ) ) {
			$file = 'single-property.php';
		} elseif ( is_post_type_archive( 'property' ) ) {
			$file = 'archive-property.php';
		} elseif ( is_singular( 'neighbourhood' ) ) {
			$file = 'single-neighbourhood.php';
		} elseif ( is_post_type_archive( 'neighbourhood' ) ) {
			$file = 'archive-neighbourhood.php';
		}

		if ( $file && file_exists( self::locate_template( $file ) ) ) {
			$template = self::locate_template( $file );
		};

		return $template;
	}

	/**
	 * @param $template_name
	 * @param string $template_path
	 * @param string $default_path
	 *
	 * @return mixed
	 */
	static function locate_template( $template_name, $template_path = '', $default_path = '' ) {
		// Set variable to search in dpg-templates folder of theme.
		if ( ! $template_path ) {
			$template_path = 'dpg-templates/';
		};
		// Set default plugin templates path.
		if ( ! $default_path ) {
			$default_path = plugin_dir_path( DPG_PLUGIN_FILE ) . 'templates/'; // Path to the template folder
		};

		// Search template file in theme folder.
		$template = locate_template( array(
			$template_path . $template_name,
			$template_name
		) );
		// Get plugins template file.
		if ( ! $template ) {
			$template = $default_path . $template_name;
		};

		return $template;
	}
}
