<?php

namespace DPG\WP\Import;

use Carbon\Carbon;
use DPG\API\API;
use DPG\WP\Admin;
use DPG\WP\Admin\Settings;
use DPG\WP\Models\Agent;
use DPG\WP\Models\Neighbourhood;
use DPG\WP\Models\Office;
use DPG\WP\Models\Property;
use DPG\WP\Plugin;
use WP_Query;

class Importer
{
    /**
     * @var API
     */
    private $api;

    /**
     * @var string - DPG API Key
     */
    public $api_key;

    /**
     * @var string - DPG agency id
     */
    public $agency_id;

    /**
     * @var string - DPG office id
     */
    public $office_id;

    /**
     * @var string - DPG agent id
     */
    public $agent_id;

    /**
     * @var string - plugin prefix
     */
    private $prefix;

    /**
     * @var int
     */
    private $limit = 20;

    /**
     * @var string - date in the format Y-m-d H:i:s in api timezone
     */
    private $last_updated;

    /**
     * @var string - date in the format Y-m-d H:i:s in api timezone
     */
    private $last_updated_offices;

    /**
     * @var string
     */
    private static $api_timezone = 'Australia/Melbourne';

    /**
     * Used when we import properties for an agent
     * and we need to make multiple api calls in
     * order to get to the 20 properties we normally
     * import per batch
     * @var int
     */
    public $agent_properties_imported = 0;

    /**
     * @var int
     */
    public $properties_updated = 0;

    /**
     * @var int
     */
    public $properties_created = 0;

    /**
     * @var array
     */
    public $agents_updated = [];

    /**
     * @var array
     */
    public $agents_created = [];

    /**
     * @var int
     */
    public $suburbs_created = 0;

    /**
     * @var int
     */
    public $offices_created = 0;

    /**
     * Importer constructor.
     */
    function __construct()
    {
        $this->prefix = Plugin::$name;

        $this->limit = 50;
        $this->api_key = Settings::get_option('api_key', 'api');
        $this->agency_id = Settings::get_option('agency_id', 'api');
        $this->office_id = Settings::get_option('office_id', 'api');
        $this->agent_id = Settings::get_option('agent_id', 'api');

        $this->properties_updated = 0;
        $this->properties_created = 0;
        $this->agent_properties_imported = 0;

        $this->api = new API($this->api_key);
    }


    /**
     * Syncs all properties
     */
    function sync()
    {
        $this->beforeSync();

        try {
            // get last synced date
            // get all properties that were updated or created since that date
            $properties = $this->getProperties();

            // if no properties got updated
            if (!$properties) {
                return [
                    'success' => true,
                    'up_to_date' => true
                ];
            }

            // update each property
            foreach ($properties as $property) {
                // if we have an agent id set, but this property doesn't belong to him, we skip it
                if ($this->agent_id && !$this->belongsToAgent($property)) {
                    continue;
                }

                $this->createOrUpdateProperty($property);
                $this->createOrUpdateNeighbourhood($property);
            }

            // set last updated
            $last_property = end($properties);
            update_option($this->prefix . '_last_update', $last_property->updated_at);

            // if fewer properties than the limit were returned, it means
            // that we reached the end;
            $up_to_date = count($properties) < $this->limit;

            // There may be a case where more properties than the limit will have the
            // same updated date. In that case, in order to not enter a infinite
            // loop (due to >= updated_date condition) we set and offset.
            if (!$up_to_date && $this->haveSameUpdatedDate($properties)) {
                $last_offset = get_option($this->prefix . '_last_offset', 0);
                update_option($this->prefix . '_last_offset', $last_offset + $this->limit);
            } else {
                delete_option($this->prefix . '_last_offset');
            }

            // flush rewrites rules
            if ($this->properties_updated || $this->properties_created) {
                flush_rewrite_rules();
            }

            return [
                'success' => true,
                'properties_updated' => $this->properties_updated,
                'properties_created' => $this->properties_created,
                'agents_updated' => count(array_unique($this->agents_updated)),
                'agents_created' => count(array_unique($this->agents_created)),
                'suburbs_created' => $this->suburbs_created ?? 0,
                'last_property_updated' => $last_property->address_full,
                'up_to_date' => $up_to_date
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }

    /**
     * Resets updated date for the properties
     */
    function syncReset()
    {
        $start_of_time = Carbon::now()->setTimestamp(0)->format('Y-m-d H:i:s');
        update_option($this->prefix . '_last_update', $start_of_time);
    }

    /**
     * Syncs all offices for the agency
     * Usually, this would only get run initially, or when a
     * new office is added, which is pretty rare
     */
    function syncOffices()
    {
        $this->beforeSync();

        try {
            $offices = $this->getOffices();

            // if no offices got updated
            if (!$offices) {
                return [
                    'success' => true,
                    'up_to_date' => true
                ];
            }

            foreach ($offices as $api_office) {
                $this->createOrUpdateOffice($api_office);
            }

            // set last updated
            $last_office = end($offices);
            update_option($this->prefix . '_last_update_offices', $last_office->updated_at);

            // if fewer properties than the limit were returned, it means
            // that we reached the end;
            $up_to_date = count($offices) < $this->limit;

            // flush rewrites rules
            if ($this->offices_created) {
                flush_rewrite_rules();
            }

            return [
                'success' => true,
                'up_to_date' => $up_to_date,
                'offices_created' => $this->offices_created
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }

    /**
     * Resets updated date for the offices
     */
    function syncOfficesReset()
    {
        $start_of_time = Carbon::now()->setTimestamp(0)->format('Y-m-d H:i:s');
        update_option($this->prefix . '_last_update_offices', $start_of_time);
    }

    /**
     * Full property data reset
     *
     * @return array
     */
    function fullReset()
    {
        $reset = [];
        $limit = 20;
        $deleted_properties = Property::deleteChunk($limit);
        if ($deleted_properties < $limit) {
            $reset['properties'] = true;
        }

        $deleted_agents = Agent::deleteChunk($limit);
        if ($deleted_agents < $limit) {
            $reset['agents'] = true;
        }

        $deleted_offices = Office::deleteChunk($limit);
        if ($deleted_offices < $limit) {
            $reset['offices'] = true;
        }

        $deleted_neighbourhoods = Neighbourhood::deleteChunk($limit);
        if ($deleted_neighbourhoods < $limit) {
            $reset['neighbourhoods'] = true;
        }

        if (count($reset) == 4) {
            $this->syncOfficesReset();
            $this->syncReset();
            return [
                'success' => true,
                'all_done' => true,
            ];
        }
        return [
            'success' => true,
            'all_done' => false,
        ];
    }

    /**
     * We get the last updated date before every sync
     */
    private function beforeSync()
    {
        $start_of_time = Carbon::now()->setTimestamp(0)->format('Y-m-d H:i:s');
        $option_last_updated = get_option($this->prefix . '_last_update');
        $option_last_updated_offices = get_option($this->prefix . '_last_update_offices');

        $this->last_updated = $option_last_updated ? $option_last_updated : $start_of_time;
        $this->last_updated_offices = $option_last_updated_offices ? $option_last_updated_offices : $start_of_time;
    }

    /**
     * @return array|mixed|null|object
     */
    private function getProperties()
    {
        $properties = null;
        $args = [
            'where' => [
                "agency_id,{$this->agency_id}",
                "updated_at,>=,{$this->last_updated}",
                "has_geolocation_issue,0"
            ],
            "orderBy" => 'updated_at,asc',
            "limit" => $this->limit,
        ];

        // Add offset when we have more properties that have the same updated date
        // then the limit we are using in order to be able to advance.
        $last_offset = get_option($this->prefix . '_last_offset');
        if ($last_offset) {
            $args['offset'] = $last_offset;
        }

        if ($this->office_id) {
            $args['where'][] = "branch_id,{$this->office_id}";
        }


        return $this->api->getProperties($args);
    }

    /**
     * @return array|mixed|null|object
     */
    private function getOffices()
    {
        $offices = null;

        $args = [
            'where' => [
                "agency_id,{$this->agency_id}",
                "updated_at,>,{$this->last_updated_offices}",
            ],
            "orderBy" => 'updated_at,asc',
            "limit" => $this->limit,
        ];
        if ($this->office_id) {
            $args['where'][] = "branch_id,{$this->office_id}";
        }
        $offices = $this->api->getBranches($args);

        return $offices;
    }

    /**
     * Because wordpress is not allowing overwriting the updated time
     */
    public function alter_post_modification_time( $data , $postarr ) {
        if (!empty($postarr['post_modified']) && !empty($postarr['post_modified_gmt'])) {
            $data['post_modified'] = $postarr['post_modified'];
            $data['post_modified_gmt'] = $postarr['post_modified_gmt'];
        }

        return $data;
    }


    /**
     * @param $api_property
     *
     * @return void
     */
    protected function createOrUpdateProperty($api_property)
    {
        // @todo check why?
        if (!strlen($api_property->address_suburb)) {
            return;
        }

        $post = Property::getPostFromApiId($api_property->property_unique_id);
        $post_data = Property::getPostDataFromApiResource($api_property, $post);

        // @todo - implement action to force on post page
//		$force_update = $found_post->found_posts && ! empty( get_field( 'field_579d956ac7a19', $found_post->posts[0]->ID ) ) ? true : false;

        add_filter( 'wp_insert_post_data', array($this,'alter_post_modification_time'), 99, 2 );
        $post_id = wp_insert_post($post_data);
        remove_filter( 'wp_insert_post_data', array($this,'alter_post_modification_time'), 99, 2 );
        $property = $post ? new Property($post) : new Property($post_id);

        // update all data
        $updated = $property->update($api_property);
        if (!$post) { // post didn't exist
            $this->properties_created++;
        } else if ($updated) { // we only update if the updated_at changed
            $this->properties_updated++;
        }

        // $updated is true when it's a new post, or when the updated_at was changed
        if ($updated) {
            // update agents
            $property_agents_ids = $this->updatePropertyAgents($api_property);

            // update agents fields for the property
            $property->updateField('property_agent', $property_agents_ids);
        }
    }

    /**
     * Checks $property data's agents, creates or updates
     * them in WordPress and updates custom field values.
     *
     * @param $api_property
     *
     * @return array
     */
    protected function updatePropertyAgents($api_property)
    {
        // Property agents
        $property_agents_ids = [];
        if ($api_property->property_agents) {

            foreach ($api_property->property_agents as $property_agent) {
                $agent = $this->createOrUpdateAgent($property_agent->agent);
                $property_agents_ids[] = $agent->post->ID;
            }
        }

        return $property_agents_ids;
    }

    /**
     * @param $api_agent
     *
     * @return Agent
     */
    protected function createOrUpdateAgent($api_agent)
    {
        $post = Agent::getPostFromApiId($api_agent->id);
        $post_data = Agent::getPostDataFromApiResource($api_agent, $post);
        $post_id = wp_insert_post($post_data);

        if ($post) {
            $agent = new Agent($post);
            // we update only if there were changes in the api
            $updated = $agent->update($api_agent);
            if ($updated) {
                $this->agents_updated[] = $agent->post->title;
            }
        } else {
            // update all fields
            $agent = new Agent($post_id);
            $agent->update($api_agent);

            $this->agents_created[] = $post_data['post_title'];
        }

        return $agent;
    }


    /**
     * @param $api_property
     *
     * @return int|void|\WP_Error
     */
    protected function createOrUpdateNeighbourhood($api_property)
    {
        if (!empty($api_property->address_suburb) && !empty($api_property->address_state && $api_property->address_postcode)) {
            $id = Neighbourhood::getUniqueId($api_property);
            $post = Neighbourhood::getPostFromApiId($id);
            $post_data = Neighbourhood::getPostDataFromApiResource($api_property, $post);

            if (!$post_data['ID']) {
                $this->suburbs_created++;
            } else {
                // @todo updated?
            }
            $post_id = wp_insert_post($post_data);

            $neighbourhood = new Neighbourhood($post_id);
            $neighbourhood->update($api_property);
        }
    }


    /**
     * @param $api_office
     */
    protected function createOrUpdateOffice($api_office)
    {
        // we shouldn't create if it was deleted
        // @todo double check this
        if ($api_office->deleted_at) {
            return;
        }

        $post = Office::getPostFromApiId($api_office->rea_agency_branch_id);
        if (!$post) {
            $post_data = Office::getPostDataFromApiResource($api_office, $post);
            $post_id = wp_insert_post($post_data);

            $office = new Office($post_id);
            $office->update($api_office);

            $this->offices_created++;
        }
    }

    /**
     * Converts local Australia/Melbourne timestamp to UTC and
     * returns both as array.
     *
     * @param  string $localTimestamp
     *
     * @return array
     */
    static function getLocalAndUtcTimes($localTimestamp)
    {
        // $date = Carbon::createFromFormat('Y-m-d H:i:s', '2017-06-30 05:48:57', 'Australia/Melbourne');
        $timestamps = [];
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $localTimestamp, self::$api_timezone);

        // get the published time for WordPress instance
        $wp_timezone = get_option('timezone_string') ? get_option('timezone_string') : get_option('gmt_offset');
        $date->setTimezone($wp_timezone);
        $timestamps['local'] = $date->format('Y-m-d H:i:s');

        // get utc time
        $date->setTimezone('UTC');
        $timestamps['utc'] = $date->format('Y-m-d H:i:s');
        $timestamps['utc_timestamp'] = $date->timestamp;

        return $timestamps;
    }

    /**
     * @param $property
     *
     * @return bool
     */
    private function belongsToAgent($property)
    {
        $belongs = false;
        $agents = $property->property_agents;
        // if this property has our agent, we add to the import list
        foreach ($agents as $agent) {
            $agent = $agent->agent; // as this is a pivot relationship response;
            if ($agent->id == $this->agent_id) {
                $belongs = true;
                break;
            }
        }

        return $belongs;
    }

    /**
     * @param $properties
     *
     * @return array
     */
    private function filterOfficesForAgent($properties): array
    {
        $agent_properties = [];
        foreach ($properties as $property) {
            $agents = $property->property_agents;
            // if this property has our agent, we add to the import list
            foreach ($agents as $agent) {
                $agent = $agent->agent; // as this is a pivot relationship response;
                if ($agent->id == $this->agent_id) {
                    $agent_properties[] = $property;
                }
            }
        }

        $properties = $agent_properties;

        return $properties;
    }

    /**
     * @param $properties
     *
     * @return bool
     */
    private function haveSameUpdatedDate($properties)
    {
        if (!$properties) {
            return false;
        }

        $first_value = $properties[0]->updated_at;
        foreach ($properties as $property) {
            if ($first_value != $property->updated_at) {
                return false;
            }
        }

        return true;
    }
}