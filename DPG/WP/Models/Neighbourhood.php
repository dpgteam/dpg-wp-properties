<?php

namespace DPG\WP\Models;

use DPG\WP\Contracts\PostType;
use DPG\WP\Stores\NeighbourhoodACFStore;
use WP_Post;
use WP_Query;

/**
 * Class Neighbourhood
 * @package DPG\WP\Models
 */
class Neighbourhood extends PostTypeModel implements PostType
{
    /**
     * @var string
     */
    private static $post_type = 'neighbourhood';

    /**
     * Neighbourhood constructor.
     *
     * @param int|WP_Post $post Optional. Post ID or WP_Post object
     */
    function __construct($post)
    {
        $this->post = $post instanceof WP_Post ? $post : get_post($post);

        $this->store = new NeighbourhoodACFStore($this->post->ID);
    }


    /**
     * @param $data
     */
    function update($data)
    {
        $this->store->update($data);
    }

    static function getPostType()
    {
        return self::$post_type;
    }


    /** Delete posts if they exists
     * @param $limit
     * @return int
     */
    static function deleteChunk($limit)
    {
        $wp_query = new \WP_Query([
            'posts_per_page' => $limit,
            'post_type' => self::$post_type,
            'post_status' => ['any', 'trash', 'auto-draft']
        ]);
        $deleted = 0;
        if ($wp_query->have_posts()) {
            while ($wp_query->have_posts()) :
                $wp_query->the_post();
                wp_delete_post(get_the_ID(), true);
                $deleted++;
            endwhile;
        }
        return $deleted;

    }

    /**
     *
     */
    static function registerPostType()
    {
        $labels = array(
            "name" => __('Neighbourhoods', ''),
            "singular_name" => __('Neighbourhood', ''),
        );

        $args = array(
            "label" => __('Neighbourhoods', ''),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "show_in_rest" => false,
            "rest_base" => "",
            "has_archive" => false,
            "show_in_menu" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array("slug" => "neighbourhood", "with_front" => false),
            "query_var" => true,
            "supports" => array("title", "editor", "thumbnail"),
            "menu_icon" => "dashicons-location"
        );

        register_post_type(self::$post_type, $args);
    }

    static function registerDataStore()
    {
        NeighbourhoodACFStore::addAcfGroup();
    }


    /**
     * Tries to create a unique id to use as we don't have one from the api
     *
     * @param $property
     *
     * @return mixed|string
     */
    static function getUniqueId($property)
    {
        $id = str_replace(' ', '_', strtolower($property->address_suburb));
        $id .= '__';
        $id .= str_replace(' ', '_', strtolower($property->address_state));
        $id .= '__';
        $id .= str_replace(' ', '_', strtolower($property->address_postcode));

        return $id;
    }

    /**
     * @param $property
     * @param $existing_post
     *
     * @return array
     */
    static function getPostDataFromApiResource($property, $existing_post)
    {
        return [
            'ID' => $existing_post->ID ?? null,
            'post_title' => ucwords(strtolower($property->address_suburb)),
            'post_type' => self::$post_type,
            'post_content' => $existing_post->post_content ?? '',
            'post_author' => 1,
            'post_status' => $existing_post->post_status ?? 'publish',
        ];
    }

    /**
     * @param $neighbourhood_unique_id
     *
     * @return null
     */
    static function getPostFromApiId($neighbourhood_unique_id)
    {
        $wp_query = new WP_Query([
            'posts_per_page' => 1,
            'post_type' => self::$post_type,
            'meta_key' => 'id',
            'meta_value' => $neighbourhood_unique_id,
            'post_status' => ['draft', 'pending', 'future', 'publish', 'private']
        ]);

        return $wp_query->posts[0] ?? null;
    }
}