<?php

namespace DPG\WP\Models;

use DPG\WP\Contracts\PostType;
use DPG\WP\Importer\Importer;
use DPG\WP\Stores\OfficeACFStore;
use DPG\WP\Stores\PropertyACFStore;
use WP_Post;
use WP_Query;

/**
 * Class Office
 * @package DPG\WP\Models
 */
class Office extends PostTypeModel implements PostType {
	/**
	 * @var string
	 */
	private static $post_type = 'office';

	/**
	 * Office constructor.
	 *
	 * @param int|WP_Post $post Optional. Post ID or WP_Post object
	 */
	function __construct( $post ) {
		$this->post = $post instanceof WP_Post ? $post : get_post( $post );

		$this->store = new OfficeACFStore( $this->post->ID );
	}


	/**
	 * @param $data
	 */
	function update( $data ) {
		$this->store->update( $data );
	}

    static function getPostType()
    {
        return self::$post_type;
    }


    /** Delete posts if they exists
     * @param $limit
     * @return int
     */
    static function deleteChunk($limit)
    {
        $wp_query = new \WP_Query([
            'posts_per_page' => $limit,
            'post_type' => self::$post_type,
            'post_status' => ['any', 'trash', 'auto-draft']
        ]);
        $deleted = 0;
        if ($wp_query->have_posts()) {
            while ($wp_query->have_posts()) :
                $wp_query->the_post();
                wp_delete_post(get_the_ID(), true);
                $deleted++;
            endwhile;
        }
        return $deleted;

    }


	/**
	 *
	 */
	static function registerPostType() {
		/**
		 * Post Type: Offices.
		 */
		$labels = array(
			"name"          => __( 'Offices', '' ),
			"singular_name" => __( 'Office', '' ),
		);

		$args = array(
			"label"               => __( 'Offices', '' ),
			"labels"              => $labels,
			"description"         => "",
			"public"              => true,
			"publicly_queryable"  => false,
			"show_ui"             => true,
			"show_in_rest"        => false,
			"rest_base"           => "",
			"has_archive"         => false,
			"show_in_menu"        => true,
			"exclude_from_search" => false,
			"capability_type"     => "post",
			"map_meta_cap"        => true,
			"hierarchical"        => false,
			"rewrite"             => array( "slug" => "office", "with_front" => false ),
			"query_var"           => true,
			"supports"            => array( "title", "editor", "thumbnail" ),
			"menu_icon"           => "dashicons-building"
		);

		register_post_type( self::$post_type, $args );
	}

	static function registerDataStore() {
		OfficeACFStore::addAcfGroup();
	}

	/**
	 * @param $branch
	 * @param $existing_post
	 *
	 * @return array
	 */
	static function getPostDataFromApiResource( $branch, $existing_post ) {
		$title = trim( str_replace( $branch->agency->name, '', $branch->name ) );

		return [
			'post_title'   => $title,
			'post_content' => '',
			'post_type'    => self::$post_type,
			'post_author'  => 1,
			// @todo - why set as draft?
			'post_status'  => 'draft',
		];
	}


	static function getPostFromApiId( $branch_id ) {
		// @todo check why the posts_per_page was 2 initially
		$wp_query = new WP_Query( [
			'posts_per_page' => 1,
			'post_type'      => self::$post_type,
			'meta_key'       => 'branch_id',
			'meta_value'     => $branch_id,
			'post_status'    => [ 'draft', 'pending', 'future', 'publish', 'private' ]
		] );

		return $wp_query->posts[0] ?? null;
	}
}