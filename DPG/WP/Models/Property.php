<?php

namespace DPG\WP\Models;

use DPG\WP\Contracts\PostType;
use DPG\WP\Import\Importer;
use DPG\WP\Stores\PropertyACFStore;
use WP_Post;
use WP_Query;

/**
 * Class Property
 * @package DPG\WP\Models
 */
class Property extends PostTypeModel implements PostType
{
    /**
     * @var string
     */
    private static $post_type = 'property';

    /**
     * Property constructor.
     *
     * @param int|WP_Post $post Optional. Post ID or WP_Post object
     */
    function __construct($post)
    {
        $this->post = $post instanceof WP_Post ? $post : get_post($post);

        $this->store = new PropertyACFStore($this->post->ID);
    }

    /**
     * @param $data
     *
     * @return bool
     */
    function update($data)
    {
        $updated_at = $this->getField('updated_at');
        if ($data->updated_at !== $updated_at) {
            $this->store->update($data);

            return true;
        }

        return false;
    }


    /**
     *
     */
    static function registerPostType()
    {
        $labels = array(
            "name" => __('Properties', ''),
            "singular_name" => __('Property', ''),
        );

        $args = array(
            "label" => __('Properties', ''),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "show_in_rest" => false,
            "rest_base" => "",
            "has_archive" => false,
            "show_in_menu" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array("slug" => "property", "with_front" => false),
            "query_var" => true,
            "supports" => array("title", "editor", "thumbnail"),
            "menu_icon" => "dashicons-admin-home"
        );

        register_post_type(self::$post_type, $args);
    }

    static function registerDataStore()
    {
        PropertyACFStore::addAcfGroup();
    }

    static function getPostType()
    {
        return self::$post_type;
    }


    /** Delete posts if they exists
     * @param $limit
     * @return int
     */
    static function deleteChunk($limit)
    {
        $wp_query = new \WP_Query([
            'posts_per_page' => $limit,
            'post_type' => self::$post_type,
            'post_status' => ['any', 'trash', 'auto-draft']
        ]);
        $deleted = 0;
        if ($wp_query->have_posts()) {
            while ($wp_query->have_posts()) :
                $wp_query->the_post();
                wp_delete_post(get_the_ID(), true);
                $deleted++;
            endwhile;
        }
        return $deleted;

    }

    /**
     * @param $property
     * @param $existing_post
     *
     * @return array
     */
    static function getPostDataFromApiResource($property, $existing_post)
    {
        $title = Property::getTitleFromApiResource($property);

        // @todo - update logic to account for the date set in the blog
        $created = Importer::getLocalAndUtcTimes($property->created_at);
        $updated = Importer::getLocalAndUtcTimes($property->updated_at);

        return [
            'ID' => $existing_post->ID ?? null,
            'post_name' => $property->rea_listing_unique_id,
            'post_title' => $title,
            'post_content' => $property->description,
            'post_type' => 'property',
            'post_author' => 1,
            'post_status' => 'publish',
            'post_date' => $created['local'],
            'post_date_gmt' => $created['utc'],
            'post_modified' => $updated['local'],
            'post_modified_gmt' => $updated['utc'],
        ];
    }


    static function getPostFromApiId($property_unique_id)
    {
        // @todo check why the posts_per_page was 2 initially
        $wp_query = new WP_Query([
            'posts_per_page' => 1,
            'post_type' => 'property',
            'meta_key' => 'id',
            'meta_value' => $property_unique_id,
            'post_status' => ['draft', 'pending', 'future', 'publish', 'private']
        ]);

        return $wp_query->posts[0] ?? null;
    }


    /**
     * @param $property
     *
     * @return string
     */
    private static function getTitleFromApiResource($property): string
    {
        $titlePieces = [];

        if ($property->address_street_number) {
            $string = $property->address_street_number;

            // Add sub number if present
            if ($property->address_sub_number) {
                $string = $property->address_sub_number . '/' . $string;
            }

            $titlePieces[] = $string;
        }

        if ($property->address_street) {
            $titlePieces[] = $property->address_street . ',';
        }

        $titlePieces[] = $property->address_suburb;
        $titlePieces[] = $property->address_postcode;

        $title = implode(' ', $titlePieces);

        return trim($title, '-');
    }
}