<?php

namespace DPG\WP\Models;

use DPG\WP\Contracts\PostType;
use DPG\WP\Import\Importer;
use DPG\WP\Stores\AgentACFStore;
use DPG\WP\Stores\PropertyACFStore;
use WP_Post;
use WP_Query;

/**
 * Class Agent
 * @package DPG\WP\Models
 */
class Agent extends PostTypeModel implements PostType
{
    /**
     * @var string
     */
    private static $post_type = 'agent';

    /**
     * Agent constructor.
     *
     * @param int|WP_Post $post Optional. Post ID or WP_Post object
     */
    function __construct($post)
    {
        $this->post = $post instanceof WP_Post ? $post : get_post($post);

        $this->store = new AgentACFStore($this->post->ID);
    }


    /**
     * @param $data
     *
     * @return bool
     */
    function update($data)
    {
        $updated_at = $this->getField('updated_at');
        if ($data->updated_at !== $updated_at) {
            $this->store->update($data);

            return true;
        }

        return false;
    }

    static function getPostType()
    {
        return self::$post_type;
    }

    /** Delete posts if they exists
     * @param $limit
     * @return int
     */
    static function deleteChunk($limit)
    {
        $wp_query = new \WP_Query([
            'posts_per_page' => $limit,
            'post_type' => self::$post_type,
            'post_status' => ['any', 'trash', 'auto-draft']
        ]);
        $deleted = 0;
        if ($wp_query->have_posts()) {
            while ($wp_query->have_posts()) :
                $wp_query->the_post();
                wp_delete_post(get_the_ID(), true);
                $deleted++;
            endwhile;
        }
        return $deleted;

    }

    /**
     *
     */
    static function registerPostType()
    {
        $labels = array(
            "name" => __('Agents', ''),
            "singular_name" => __('Agent', ''),
        );

        $args = array(
            "label" => __('Agents', ''),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "show_in_rest" => false,
            "rest_base" => "",
            "has_archive" => false,
            "show_in_menu" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array("slug" => "agent", "with_front" => false),
            "query_var" => true,
            "supports" => array("title", "editor", "thumbnail"),
            "menu_icon" => "dashicons-businessman"
        );

        register_post_type(self::$post_type, $args);
    }

    static function registerDataStore()
    {
        return AgentACFStore::addAcfGroup();
    }

    /**
     * @param $agent
     * @param $existing_post
     *
     * @return array
     */
    static function getPostDataFromApiResource($agent, $existing_post)
    {
        $title = ucwords($agent->name);

        // @todo - update logic to account for the date set in the blog
        $created = Importer::getLocalAndUtcTimes($agent->created_at);
        $updated = Importer::getLocalAndUtcTimes($agent->updated_at);

        return [
            'ID' => $existing_post->ID ?? null,
            'post_title' => $title,
            'post_type' => self::$post_type,
            'post_content' => $existing_post->post_content ?? '',
            'post_status' => $existing_post->post_status ?? 'publish',
            'post_author' => 1,
            'post_date' => $created['local'],
            'post_date_gmt' => $created['utc']
        ];
    }


    static function getPostFromApiId($agent_id)
    {
        // @todo check why the posts_per_page was 2 initially
        $wp_query = new WP_Query([
            'posts_per_page' => 1,
            'post_type' => self::$post_type,
            'meta_key' => 'id',
            'meta_value' => $agent_id,
            'post_status' => ['draft', 'pending', 'future', 'publish', 'private']
        ]);

        return $wp_query->posts[0] ?? null;
    }
}