<?php

namespace DPG\WP\Models;

use DPG\WP\Contracts\DataStore;
use WP_Post;

abstract class PostTypeModel
{
    /**
     * @var WP_Post
     */
    public $post;


    /**
     * @var DataStore
     */
    protected $store;

    /**
     * @param $field
     *
     * @return mixed
     */
    function getField($field)
    {
        return $this->store->getField($field);
    }

    /**
     * @param $field
     * @param $value
     */
    function updateField($field, $value)
    {
        $this->store->updateField($field, $value);
    }


}