<?php

namespace DPG\WP\Ajax;


use DPG\SingleAgent\Help;
use WP_Mail;

class Manager
{
    public function __construct()
    {

        add_action('wp_ajax_contact_agent', array($this, 'ajax_email_agent'));
        add_action('wp_ajax_nopriv_contact_agent', array($this, 'ajax_email_agent'));
    }

    public function ajax_email_agent()
    {
        if (!$this->validate_contact_agent()) {
            echo $this->response('Please fill in all required fields');
            die();
        }
        $agent_email = $_POST['agent']['email'];
        $site_name = get_bloginfo('name');
        $email = WP_Mail::init()
            ->to($agent_email)
            ->subject('New enquire from ' . $site_name)
            ->template(Help::getPartial('emails/contact-agent', [], true), array_merge($_POST, [
                'sitename' => $site_name
            ]))->send();
        if ($email) {
            echo $this->response('Mail successfully sent');
        } else {
            echo $this->response('There has been a problem contacting the agent, please try again.');
        }
        die();

    }


    private function validate_contact_agent()
    {
        if (!$_POST['email'] || !$_POST['name'] || !$_POST['agent'] || !$_POST['property'] || !$_POST['agency']) {
            return false;
        }
        return true;
    }

    private function response($message)
    {
        return wp_json_encode(['message' => $message]);
    }


}