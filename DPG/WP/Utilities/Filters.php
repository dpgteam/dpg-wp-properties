<?php

namespace DPG\WP\Utilities;

use DPG\WP\Admin\Settings;
use DPG\WP\Plugin;

class Filters
{


    /**
     * Defaults constructor.
     */
    public function __construct()
    {

        add_filter('gform_pre_send_email', array($this, 'before_email'));
    }

    /**
     * convert encoded html in links back to standard
     */
    function before_email($email)
    {
        $email['message'] = str_replace('&amp;', '&', $email['message']);
        return $email;
    }


}