<?php

namespace DPG\WP\Utilities;

use DPG\WP\Admin\Settings;
use DPG\WP\Plugin;

class Defaults
{


    /**
     * Defaults constructor.
     */
    public function __construct()
    {

        add_filter('template_redirect', array($this, 'set_agent_default_thumbnail'));
    }

    /**
     * If the single page is viewed, update the post featured image if this doesn't exist with the default
     */
    function set_agent_default_thumbnail()
    {
        if (!(is_single() && get_post_type(get_the_ID()) == 'agent')) return;

        if (has_post_thumbnail()) return;

        $image = Settings::get_option('default_agent_image', 'frontend');
        if (!$image) return;

        $thumb_id = $this->aws_get_image_id($image);
        update_post_meta(get_the_ID(), '_thumbnail_id', $thumb_id);
    }

    /**
     * Filter image url trough aws
     *
     * @param $image_url
     * @return null
     */
    function aws_get_image_id($image_url)
    {
        global $wpdb;
        $image_url = apply_filters('as3cf_filter_post_s3_to_local', "<img src='" . $image_url . "'/>");
        $image_url = str_replace('<img src=\'', '', $image_url);
        $image_url = str_replace('\'/>', '', $image_url);
        if (strpos(get_bloginfo('url'), 'http://') !== false) {
            $image_url = str_replace('https://', 'http://', $image_url);
        }

        $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url));
        return $attachment[0] ?? null;
    }


}