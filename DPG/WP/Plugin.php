<?php

namespace DPG\WP;

use DPG\WP\Admin\Admin;
use DPG\WP\Admin\ScssCompiler;
use DPG\WP\Admin\Settings;
use DPG\WP\Ajax\Manager;
use DPG\WP\API\Controller;
use DPG\WP\Models\Agent;
use DPG\WP\Models\Neighbourhood;
use DPG\WP\Models\Office;
use DPG\WP\Models\Property;
use DPG\WP\Traits\AddsFiltersAndActions;
use DPG\WP\Utilities\Defaults;
use DPG\WP\Utilities\Filters;
use DPG\WP\VC\NeighbourhoodsShortcode;
use DPG\WP\VC\PropertiesLatestShortcode;
use DPG\WP\VC\PropertiesShortcode;
use DPG\WP\VC\UpcomingInspectionsAuctionsShortcode;
use DPG\WP\VC\PricefinderShortcode;

final class Plugin
{

    use AddsFiltersAndActions;

    /**
     * @var string
     */
    public $version = '1.0.0';

    /**
     * @var string
     */
    public static $name = 'dpg_wp_properties';

    /**
     * The single instance of the class.
     * @var Plugin
     * @since 2.1
     */
    protected static $_instance = null;

    /**
     * Main Plugin Instance.
     * @static
     * @return Plugin - Main instance.
     */
    public static function instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Plugin Constructor.
     */
    public function __construct()
    {
        $this->initHooks();
    }

    public function initHooks()
    {

        // adds the needed hooks in the constructor;
        new Admin();

        // add ajax calls
        new Manager();

        new Defaults();
        new Filters();

        // add template loader hooks
        TemplateLoader::init();

        // add shortcodes
        PropertiesShortcode::init();
        PropertiesLatestShortcode::init();
        NeighbourhoodsShortcode::init();
        UpcomingInspectionsAuctionsShortcode::init();
        PricefinderShortcode::init();

        // hooks
        add_action('init', array(Property::class, 'registerPostType'));
        add_action('acf/init', array(Property::class, 'registerDataStore'));

        add_action('init', array(Agent::class, 'registerPostType'));
        add_action('acf/init', array(Agent::class, 'registerDataStore'));

        add_action('init', array(Office::class, 'registerPostType'));
        add_action('acf/init', array(Office::class, 'registerDataStore'));

        add_action('init', array(Neighbourhood::class, 'registerPostType'));
        add_action('acf/init', array(Neighbourhood::class, 'registerDataStore'));


        add_action('rest_api_init', array(Controller::class, 'registerRoutes'));

        // enqueue scripts
        add_action('wp_enqueue_scripts', array($this, 'enqueue'));
        add_action('admin_enqueue_scripts', array($this, 'adminEnqueue'));

        // adds container for the SE widget
        add_action('wp_footer', array($this, 'addSEWidgetContainer'));

        // add google map key
        add_filter('acf/fields/google_map/api', array($this, 'acfGoogleMapKey'));
    }

    public function enqueue()
    {
        if (!$this->shouldEnqueueAssets()) {
            return;
        }

        $path = plugin_dir_url(DPG_PLUGIN_FILE);
        $google_map_key = Settings::get_option('google_map_key', 'api');

        wp_enqueue_style(self::$name, $path . 'public/css/main.css', [
            'main-styles' // salient theme styles
        ], $this->version, false);
        wp_enqueue_script('google/map', "https://maps.googleapis.com/maps/api/js?libraries=places&key={$google_map_key}", [], null, true);

        wp_register_script(self::$name, $path . 'public/js/main.js', array(
            'jquery',
            'google/map'
        ), $this->version, true);
        wp_localize_script(self::$name, self::$name, $this->getLocalizationData());
        wp_enqueue_script(self::$name);
    }

    public function adminEnqueue()
    {
        $path = plugin_dir_url(DPG_PLUGIN_FILE);
        wp_register_script(self::$name, $path . 'public/js/admin.js', array('jquery'), $this->version, true);
        wp_localize_script(self::$name, self::$name, $this->getLocalizationData());
        wp_enqueue_script(self::$name);
    }


    public function addSEWidgetContainer()
    {
        echo '<div id="widget-area" style="display:none;"></div>';
    }

    /**
     * Checks if we should load frontend assets
     * @return bool
     */
    private function shouldEnqueueAssets()
    {
        global $post;

        // check if VC shortcode was used
        if (is_a($post, 'WP_Post') && (has_shortcode($post->post_content, 'dpg-properties') || has_shortcode($post->post_content, 'dpg-neighbourhoods') || has_shortcode($post->post_content, 'upcoming-inspections-auctions') || has_shortcode($post->post_content, 'dpg-latest-properties') || has_shortcode($post->post_content, 'dpg-pricefinder'))) {
            return true;
        }

        return is_singular('property') || is_post_type_archive('property') || is_singular('neighbourhood') || is_post_type_archive('neighbourhood');
    }

    private function getLocalizationData()
    {
        return [
            'baseUrl' => home_url('/'),
            'ajaxurl' => admin_url('admin-ajax.php'),
            'apiEndpoint' => Controller::get_rest_url('/import'),
            'primaryColor' => Settings::get_option('primary_color', 'frontend'),
            'mapMarkerColor' => Settings::get_option('map_marker_color', 'frontend'),
        ];
    }

    public function acfGoogleMapKey($api)
    {
        $api['key'] = Settings::get_option('google_map_key', 'api');

        return $api;
    }


}
