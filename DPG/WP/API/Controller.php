<?php

namespace DPG\WP\API;

use DPG\WP\Import\Importer;
use WP_REST_Server;

class Controller {
	public static $version = '1';
	public static $namespace = 'dpg/v1';

	public static function registerRoutes() {
		$importer = new Importer();
		if ( $importer->api_key && $importer->agency_id ) {
			register_rest_route( self::$namespace, '/import/sync', array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $importer, 'sync' ),
			) );

			register_rest_route( self::$namespace, '/import/sync/reset', array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $importer, 'syncReset' ),
			) );

			register_rest_route( self::$namespace, '/import/sync-offices', array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $importer, 'syncOffices' ),
			) );

			register_rest_route( self::$namespace, '/import/sync-offices/reset', array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $importer, 'syncOfficesReset' ),
			) );


		}
        register_rest_route( self::$namespace, '/import/full-reset', array(
            'methods'  => WP_REST_Server::READABLE,
            'callback' => array( $importer, 'fullReset' ),
        ) );
	}

	public static function get_rest_url( $path ) {
		return get_rest_url( null, self::$namespace . $path );
	}
}