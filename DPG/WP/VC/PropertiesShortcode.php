<?php

namespace DPG\WP\VC;

class PropertiesShortcode {
	static function init() {
		add_shortcode( 'dpg-properties', array( __CLASS__, 'shortcode' ) );
		add_action( 'vc_before_init', array( __CLASS__, 'addVC' ) );
	}

	/**
	 * Properties shortcode
	 * @return string
	 */
	static function shortcode() {
		$file_path = plugin_dir_path( DPG_PLUGIN_FILE ) . 'templates/shortcodes/properties.php';

		ob_start();
		include( $file_path );
		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	/**
	 * Adds shortcode the VC editor
	 */
	static function addVC() {
		if ( ! function_exists( 'vc_map' ) ) {
			return;
		}

		vc_map( array(
			"name"     => "DPG Properties",
			"base"     => "dpg-properties",
			"class"    => "",
			"category" => "Content"
		) );
	}
}
