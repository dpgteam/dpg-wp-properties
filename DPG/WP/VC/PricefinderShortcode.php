<?php

namespace DPG\WP\VC;

class PricefinderShortcode {
	static function init() {
		add_shortcode( 'dpg-pricefinder', array( __CLASS__, 'shortcode' ) );
		add_action( 'vc_before_init', array( __CLASS__, 'addVC' ) );
	}

	/**
	 * Pricefinder shortcode
	 * @return string
	 */
	static function shortcode() {
		$file_path = plugin_dir_path( DPG_PLUGIN_FILE ) . 'templates/shortcodes/pricefinder.php';

		ob_start();
		include( $file_path );
		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	/**
	 * Adds shortcode the VC editor
	 */
	static function addVC() {
		if ( ! function_exists( 'vc_map' ) ) {
			return;
		}

		vc_map( array(
			"name"     => "DPG Pricefinder",
			"base"     => "dpg-pricefinder",
			"class"    => "",
			"category" => "Content"
		) );
	}
}
