<?php

namespace DPG\WP\Admin;

use DPG\WP\Plugin;
use Leafo\ScssPhp\Compiler;

class ScssCompiler {
	/**
	 * @var Compiler
	 */
	private $scss;

	/**
	 * @var string
	 */
	private $plugin_path;

	/**
	 * Constructor
	 */
	public function __construct() {
		$scss = new Compiler();
		$this->scss = $scss;
		$this->plugin_path = plugin_dir_path( DPG_PLUGIN_FILE );
	}

	/**
	 * Compile .scss file
	 *
	 * @param string $in Input file (.scss)
	 * @param string $out Output file (.css) optional
	 *
	 * @return string|bool
	 * @throws \Exception
	 */
	private function compileFile( $in, $out = null ) {
		if ( ! is_readable( $in ) ) {
			throw new \Exception( 'load error: failed to find ' . $in );
		}

		$pi = pathinfo( $in );

		$this->scss->addImportPath( $pi['dirname'] . '/' );

		$compiled = $this->scss->compile( file_get_contents( $in ), $in );

		if ( $out !== null ) {
			return file_put_contents( $out, $compiled );
		}

		return $compiled;
	}

	/**
	 * Compiles the main sass file
	 */
	public function compile( $options ) {
		$vars = [];

		$vars['font-url'] = '"https://fonts.googleapis.com/css?family=PLM:300,400,600,700"';
		foreach ( $options as $key => $option ) {
			if ( $option ) {
				$key = str_replace( '_', '-', $key );
				if ( $key == 'font-family' ) {
					$vars['font-url'] = '"https://fonts.googleapis.com/css?family=' . str_replace( " ", "+", $option ) . ':300,400,600,700"';
					$option .= ' , sans-serif';
				}
				$vars[ $key ] = $option;
			}
		}


		$in = $this->plugin_path . 'assets/scss/main.scss';
		$out = $this->plugin_path . 'public/css/main.css';
		$this->scss->setVariables( $vars );
		$this->compileFile( $in, $out );

//		print_r($vars);
//		die();
		// save hash in order to re-compile when we make updates
		// to the css and then we deploy
		$hash = hash_file( 'md5', $out );
		update_option( Plugin::$name . '_style_hash', $hash );
	}
}