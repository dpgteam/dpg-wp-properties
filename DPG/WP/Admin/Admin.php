<?php

namespace DPG\WP\Admin;

use DPG\WP\Plugin;
use DPG\WP\Traits\AddsFiltersAndActions;
use WeDevs_Settings_API;

class Admin {
	/**
	 * @var Settings
	 */
	private $settings;

	/**
	 * @var ScssCompiler
	 */
	private $scss_compiler;

	public function __construct() {
		$this->settings      = new Settings();
		$this->scss_compiler = new ScssCompiler();

		add_action( 'admin_init', array( $this, 'admin_init' ) );
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
		add_action( 'wp_loaded', array( $this, 'wp_loaded' ) );

		add_action( 'updated_option', array( $this, 'options_updated' ), $priority = 10, $accepted_args = 3 );
	}

	function admin_menu() {
		add_menu_page( 'DPG Properties', 'DPG Properties', 'delete_posts', 'dpg_properties', array(
			$this,
			'render'
		) );
		add_submenu_page( 'dpg_properties', 'Settings', 'Settings', 'delete_posts', 'dpg_properties_settings', array(
			$this->settings,
			'settings_page'
		) );
	}

	function admin_init() {
		//set the settings
		$this->settings->settings_api->set_sections( $this->settings->get_settings_sections() );
		$this->settings->settings_api->set_fields( $this->settings->get_settings_fields() );
		//initialize settings
		$this->settings->settings_api->admin_init();
	}

	function wp_loaded(){
		// run scss if we deployed a new version of css
		$file       = plugin_dir_path( DPG_PLUGIN_FILE ) . 'public/css/main.css';
		$hash       = hash_file( 'md5', $file );
		$saved_hash = get_option( Plugin::$name . '_style_hash' );
		if ( $hash != $saved_hash ) {
			$this->scss_compiler->compile( get_option( Plugin::$name . '_frontend' ) );
		}
	}

	/**
	 * @param $option
	 * @param $old_value
	 * @param $value
	 */
	function options_updated( $option, $old_value, $value ) {
		if ( $option == Plugin::$name . '_frontend' ) {
			$this->scss_compiler->compile( $value );
		}
	}

	/**
	 * Renders view for the admin page
	 */
	public function render() {
		$path = plugin_dir_path( DPG_PLUGIN_FILE );
		include $path . 'views/admin/index.php';
	}
}