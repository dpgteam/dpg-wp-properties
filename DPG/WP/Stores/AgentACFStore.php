<?php

namespace DPG\WP\Stores;

use StoutLogic\AcfBuilder\FieldsBuilder;

class AgentACFStore extends ACFStore {
	/**
	 * @var string
	 */
	protected $field_prefix = 'field_agent_';

	/**
	 * Saves agent data
	 *
	 * @param $agent
	 */
	function update( $agent ) {
		$this->updateField( 'id', $agent->id );
		$this->updateField( 'mobile', $agent->mobile ?? '' );
		$this->updateField( 'phone', $agent->phone ?? '' );
		$this->updateField( 'created_at', $agent->created_at );
		$this->updateField( 'updated_at', $agent->updated_at );
		$this->updateField( 'email', $this->get_email( $agent ) );
	}

	function get_email( $agent ) {
		if ( empty( $agent->email_aliases[0]->email ) ) {
			return $agent->email;
		}
		end( $agent->email_aliases );         // move the internal pointer to the end of the array
		$key = key( $agent->email_aliases );
		if ( strpos( $agent->email_aliases[ $key ]->email, 'digitalpropertygroup.com' ) !== false ) {
			return $agent->email;
		}

		return $agent->email_aliases[ $key ]->email;
	}


	protected static function buildFields() {
		$fields = new FieldsBuilder( 'agent' );

		self::generalTab( $fields );
		self::testimonialsTab( $fields );

		$fields->setLocation( 'post_type', '==', 'agent' );

		return $fields;
	}

	/**
	 * @param $fields FieldsBuilder
	 */
	private static function generalTab( $fields ) {
		$fields
			->addTab( 'General', [
				'placement' => 'left'
			] )
			->addText( 'title' )// field_5777bc707d486
			->addText( 'id', [
				'wrapper'  => [ 'width' => 25 ],
				'readonly' => 1
			] )// field_576f495abea01
			->addText( 'phone', [ 'wrapper' => [ 'width' => 25 ] ] )// field_57443bae4937c
			->addText( 'mobile', [ 'wrapper' => [ 'width' => 25 ] ] )// field_57443b884937b
			->addText( 'email', [ 'wrapper' => [ 'width' => 25 ] ] )// field_57443b794937a
			->addImage( 'photo' )// field_57750d5068936
			->addUrl( 'local_video' )// field_598aa0652b5b9
			->addRadio( 'video_type', [
				'wrapper'       => 50,
				'choices'       => [
					'youtube' => 'YouTube',
					'vimeo'   => 'Vimeo'
				],
				'default_value' => 'youtube',
				'layout'        => 'horizontal'
			] )// field_577ba263b236a
			->addText( 'video_id', [ 'wrapper' => 50 ] )// field_577ba2b82dce5
			->addRepeater( 'additional_videos', [
				'layout' => 'table',
			] )
			->addRadio( 'video_type', [
				'choices'       => [
					'youtube' => 'YouTube',
					'vimeo'   => 'Vimeo'
				],
				'default_value' => 'youtube',
				'layout'        => 'horizontal'
			] )// field_57e09426cf912
			->addText( 'video_id', [ 'wrapper' => 50 ] )// field_57e09464cf913
			->endRepeater()
			->addRelationship( 'property_agent', [
				'label'     => 'Properties',
				'post_type' => array(
					0 => 'property',
				),
				'filters'   => array(
					0 => 'search',
				)
			] )// field_5774efcb3dc7e
			->addRelationship( 'office_principal', [
				'label'     => 'Principal of Office',
				'post_type' => array(
					0 => 'office',
				),
				'filters'   => array(
					0 => 'search',
				)
			] )// field_5774eadd55b29
			->addRelationship( 'office_salesagent', [
				'label'     => 'Sales Agent of Office',
				'post_type' => array(
					0 => 'office',
				),
				'filters'   => array(
					0 => 'search',
				)
			] )// field_5774ed8c21677
			->addRelationship( 'office_propertymanager', [
				'label'     => 'Property Manager of Office',
				'post_type' => array(
					0 => 'office',
				),
				'filters'   => array(
					0 => 'search',
				)
			] )// field_5774ee2821678
			->addRelationship( 'office_admin', [
				'label'     => 'Admin of Office',
				'post_type' => array(
					0 => 'office',
				),
				'filters'   => array(
					0 => 'search',
				)
			] )// field_577a52c0fffd8
			->addText( 'created_at', [
				'wrapper'  => [ 'width' => 50 ],
				'readonly' => 1
			] )// field_576f48c7c6c04
			->addText( 'updated_at', [
				'wrapper'  => [ 'width' => 50 ],
				'readonly' => 1
			] ); // field_576f48cfc6c05
	}

	/**
	 * @param $fields FieldsBuilder
	 */
	private static function testimonialsTab( $fields ) {
		$fields
			->addTab( 'Testimonials', [
				'placement' => 'left'
			] )
			->addText( 'trust_pilot_tags' )// field_5994f5b8c443a
			->addRepeater( 'testimonials', [
				'layout'       => 'row',
				'button_label' => 'Add Testimonial'
			] )// field_57443ccd4937d
			->addText( 'client' )// field_57443db34937e
			->addWysiwyg( 'details', [
				'media_upload' => 0
			] )// field_57443dc64937f
			->endRepeater();
	}
}