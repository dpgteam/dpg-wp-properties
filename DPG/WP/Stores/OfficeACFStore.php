<?php

namespace DPG\WP\Stores;

use StoutLogic\AcfBuilder\FieldsBuilder;

class OfficeACFStore extends ACFStore {
	/**
	 * @var string
	 */
	protected $field_prefix = 'field_office_';

	/**
	 * Saves office data
	 *
	 * @param $office
	 */
	function update( $office ) {
		$this->updateField( 'branch_id', $office->rea_agency_branch_id );
	}

	/**
	 * @return FieldsBuilder
	 */
	protected static function buildFields() {
		$fields = new FieldsBuilder( 'office' );

		self::generalTab( $fields );
		self::resourcesTab( $fields );
		self::ourTeamTab( $fields );

		$fields->setLocation( 'post_type', '==', 'office' );

		return $fields;
	}

	/**
	 * @param $fields FieldsBuilder
	 */
	private static function generalTab( $fields ) {
		$fields
			->addTab( 'General', [
				'placement' => 'left'
			] )
			->addText( 'branch_id', [
				'readonly' => 1
			] )
			->addImage( 'thumbnail_image' )
			->addText( 'phone' )
			->addEmail( 'email' )
			->addText( 'inspect_account',['label'=>'Inspect Real Estate Account'] )
			->addText( 'opening_hours' )
			->addRelationship( 'office_principal', [
				'label'     => 'Principals',
				'post_type' => array(
					0 => 'agent',
				),
				'filters'   => array(
					0 => 'search',
				),
			] )
			->addGoogleMap( 'google_map', [
				'label'    => 'Location',
				'required' => 1,
			] )
			->addText( 'trust_pilot_tags' );
	}

	/**
	 * @param $fields FieldsBuilder
	 */
	private static function resourcesTab( $fields ) {
		$fields
			->addTab( 'Resources', [
				'placement' => 'left'
			] )
			->addFlexibleContent( 'resources', [
				'button_label' => 'Add Resource',
			] )
			->addLayout( 'file', [
				'display' => 'block'
			] )
			->addText( 'label', [
				'required' => 1,
				'wrapper'  => [
					'width' => 30
				]
			] )
			->addFile( 'file', [
				'required' => 1,
				'wrapper'  => [
					'width' => 70
				],
				'library'  => 'uploadedTo',
			] )
			->addLayout( 'url', [
				'display' => 'block'
			] )
			->addText( 'label', [
				'required' => 1,
				'wrapper'  => [
					'width' => 30
				]
			] )
			->addUrl( 'url', [
				'required' => 1,
				'wrapper'  => [
					'width' => 70
				],
				'library'  => 'uploadedTo',
			] )
			->endFlexibleContent();
	}

	/**
	 * @param $fields FieldsBuilder
	 */
	private static function ourTeamTab( $fields ) {
		$fields
			->addTab( 'Our Team', [
				'placement' => 'left'
			] )
			->addRelationship( 'office_salesagent', [
				'label'     => 'Sales Agents',
				'post_type' => array(
					0 => 'agent',
				),
				'filters'   => array(
					0 => 'search',
				),
			] )
			->addRelationship( 'office_propertymanager', [
				'label'     => 'Property Managers',
				'post_type' => array(
					0 => 'agent',
				),
				'filters'   => array(
					0 => 'search',
				),
			] )
			->addRelationship( 'office_admin', [
				'label'     => 'Administration',
				'post_type' => array(
					0 => 'agent',
				),
				'filters'   => array(
					0 => 'search',
				),
			] );
	}
}