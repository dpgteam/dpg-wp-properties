<?php

namespace DPG\WP\Stores;

use DPG\WP\Contracts\DataStore;
use DPG\WP\Traits\AddsFiltersAndActions;
use StoutLogic\AcfBuilder\FieldsBuilder;

class PropertyACFStore extends ACFStore
{
    /**
     * @var string
     */
    protected $field_prefix = 'field_property_';

    /**
     * Updates property data
     *
     * @param $property
     */
    public function update($property) {
        $this->updateField('id', $property->property_unique_id);
        $this->updateField('branch_id', $property->rea_agency_id);
        $this->updateField('headline',$property->headline);
        $this->updateField('property_type', $property->property_type);
        $this->updateField('property_status', $property->property_status);
        $this->updateField('property_commercial_listing_type', $property->commercial_listing_type);
        $this->updateField('video_link', $property->video_link);
        $this->updateField('address_display', $property->address_display);
        $this->updateField('address_full', $property->address_full);
        $this->updateField('address_sub_number', $property->address_sub_number);
        $this->updateField('address_street_number', $property->address_street_number);
        $this->updateField('address_street', $property->address_street);
        $this->updateField('address_suburb', $property->address_suburb);
        $this->updateField('address_state', $property->address_state);
        $this->updateField('address_postcode', $property->address_postcode);
        $this->updateField('lat', $property->lat);
        $this->updateField('lon', $property->lon);
        $this->updateField('auction_date', $property->auction_date);
        $this->updateField('created_at', $property->created_at);
        $this->updateField('updated_at', $property->updated_at);

        $this->updatePropertyCategory($property);
        $this->updatePropertyObjects($property);
        $this->updatePropertyPricing($property);
        $this->updatePropertyInspectionTimes($property);
        $this->updatePropertyFeatureFields($property);
        $this->updatePropertyImages($property);
        $this->updateProperty3DfloorPlan($property);

		if (strpos($property->headline, 'OFF MARKET') !== false) {
			$this->updateField('property_type', 'off-market');
		}
    }

    /**
     * Determines the appropriate property category and updates custom field value.
     *
     * @param object $property
     *
     * @return void
     */
    private function updatePropertyCategory($property) {
        if ( ! empty($property->category)) {
            $this->updateField('category', $property->category);
        } elseif ( ! empty($property->property_commercial_categories)) {
            $this->updateField('category', $property->property_commercial_categories[0]->commercial_category);
        }
    }

    /**
     * Updates floor plan and property media if there is found data.
     *
     * @param object $property
     *
     * @return void
     */
    private function updatePropertyObjects($property) {
        // Property Media
        if ( ! empty($property->property_media)) {
            $this->updateField('property_media', $property->property_media[0]->url);
        }
        // Property objects

        if ( ! empty($property->property_objects)) :
            $first_floor_plan  = '';
            $second_floor_plan = '';
            foreach ($property->property_objects as $p) :
                if ($p->object_type == 'floorplan') {
                    if ( ! $first_floor_plan) {
                        $first_floor_plan = $p->object_url;
                    } elseif ( ! $second_floor_plan) {
                        $second_floor_plan = $p->object_url;
                    }

                }
            endforeach;
            if ($second_floor_plan) {
                $this->updateField('floor_plan', $second_floor_plan ?: '');
                $this->updateField('3d_floor_plan_image', $first_floor_plan ?: '');
            } elseif ($first_floor_plan) {
                $this->updateField('floor_plan', $first_floor_plan ?: '');
            }

        endif;


    }

    /**
     * Updates property pricing data.
     *
     * @param Object $property
     *
     * @return void
     */
    private function updatePropertyPricing($property) {
        $this->updateField('price', $property->price);
        $this->updateField('price_display', $property->price_display);
        $this->updateField('price_view', $property->price_view);
        $this->updateField('under_offer', $property->under_offer);
        $this->updateField('bond', $property->bond);
        $this->updateField('date_available', $property->date_available);
        if ( ! empty($property->property_rents[0])) {
            $this->updateField('rent_amount', $property->property_rents[0]->rent_amount ?: '');
            $this->updateField('rent_period', $property->property_rents[0]->rent_period ?: '');
        } elseif ( ! empty($property->commercial_rent)) {
            $this->updateField('rent_amount', $property->commercial_rent ?: '');
            $this->updateField('rent_period', $property->commercial_rent_period ?: '');
        }

        // Sold details
        if ( ! empty($property->property_sold_details[0])) {
            $this->updateField('sold_date', $property->property_sold_details[0]->sold_date ?: '');
            $this->updateField('sold_price', $property->property_sold_details[0]->sold_price ?: '');
            $this->updateField('display_sold_price', $property->property_sold_details[0]->display_sold_price ?: '');
        }
    }

    /**
     * Updates property inspection times.
     *
     * @param Object $property
     *
     * @return void
     */
    private function updatePropertyInspectionTimes($property) {
        // @todo - check if this is the right logic, as this is copied from brad teal
        // but b&s seems to be handling it better (or is it just in the frontend?)
        if ( ! empty($property->property_inspection_times)) {
            $times = [];
            foreach ($property->property_inspection_times as $time) {
                $t = explode(' ', $time->inspection_time);

                $times[] = [
                    'label'           => $time->inspection_time,
                    'timestamp_start' => strtotime($t[0] . ' ' . $t[1]),
                    'timestamp_end'   => strtotime($t[0] . ' ' . $t[3])
                ];
            }
            $this->updateField('inspection_times', $times);
        } else {
            $this->updateField('inspection_times', []);
        }
    }

    /**
     * Extracts property features from $property and saves field values.
     *
     * @param  $property
     *
     * @return void
     */
    private function updatePropertyFeatureFields($property) {
        $car_spaces = 0;
        foreach ($property->property_features as $feature) :
            $field = '';
            switch ($feature->feature_name) {
                case 'Bedrooms':
                    $field = 'bedrooms';
                    break;
                case 'Bathrooms':
                    $field = 'bathrooms';
                    break;
                case 'Garages':
                    $field      = 'garages';
                    $car_spaces += $feature->feature_value;
                    break;
                case 'Carports':
                    $field      = 'carports';
                    $car_spaces += $feature->feature_value;
                    break;
                case 'Open Spaces':
                    $field      = 'open_spaces';
                    $car_spaces += $feature->feature_value;
                    break;
                case 'Living Areas':
                    $field = 'living_areas';
                    break;
                case 'Study':
                    $field = 'study';
                    break;
                case 'Workshop':
                    $field = 'workshop';
                    break;
                case 'Pay T V':
                    $field = 'pay_tv';
                    break;
                case 'Pool Above Ground':
                    $field = 'pool_above_ground';
                    break;
                case 'Ducted Heating':
                    $field = 'ducted_heating';
                    break;
                case 'Air Conditioning':
                    $field = 'air_conditioning';
                    break;
                case 'Floorboards':
                    $field = 'floorboards';
                    break;
                case 'Tennis Court':
                    $field = 'tennis_court';
                    break;
                case 'Built In Robes':
                    $field = 'built_in_robes';
                    break;
                case 'Pool In Ground':
                    $field = 'pool_in_ground';
                    break;
                case 'Open Fire Place':
                    $field = 'open_fire_place';
                    break;
                case 'Intercom':
                    $field = 'intercom';
                    break;
                case 'Ducted Cooling':
                    $field = 'ducted_cooling';
                    break;
                case 'Dishwasher':
                    $field = 'dishwasher';
                    break;
                case 'Inside Spa':
                    $field = 'inside_spa';
                    break;
                case 'Outside Spa':
                    $field = 'outside_spa';
                    break;
            }
            $this->updateField($field, $feature->feature_value);
        endforeach;

        //land surface
        if (isset($property->property_land_details[0]->land_attribute_value)) {
            $this->updateField('land_area', $property->property_land_details[0]->land_attribute_value);
        }

        if (isset($property->property_land_details[0]->land_attribute_unit)) {
            $this->updateField('land_area_unit', $property->property_land_details[0]->land_attribute_unit);
        }

        // Calculate car spaces (car_spaces + carports + garages)
        $this->updateField('car_spaces', $car_spaces);
    }

    /**
     * Updates property images.
     *
     * @param Object $property
     *
     * @return void
     */
    private function updatePropertyImages($property) {
        if ( ! empty($property->property_images)) {
            $images = [];
            foreach ($property->property_images as $image) {
                $images[] = ['url' => $image->url];
            }
            $this->updateField('images', $images);
        }
    }


    /**
     * Updates property 3d floor plan instead of vide.
     *
     * @param Object $property
     *
     * @return void
     */
    private function updateProperty3DfloorPlan($property) {
        if ( ! empty($property->property_external_links)) {
            foreach ($property->property_external_links as $link) {
                if (strpos($link->url, 'diakrit.com') !== -1) {
                    $this->updateField('3d_floor_plan', $link->url);
                }
            }
        }
    }

    /**
     * Creates the fields for the properties
     */
    protected static function buildFields() {
        $property_fields = new FieldsBuilder('property');

        self::generalTab($property_fields);
        self::imagesTab($property_fields);
        self::resourcesTab($property_fields);
        self::featuresTab($property_fields);

        $property_fields->setLocation('post_type', '==', 'property');

        return $property_fields;
    }

    /**
     * @param $fields FieldsBuilder
     */
    private static function generalTab($fields) {
        $fields
            ->addTab('General', [
                'placement' => 'left'
            ])
//			->addTrueFalse( 'force_update', [
//				'instructions' => 'Force this property to be updated',
//			] )// field_579d956ac7a19
            ->addText('id', [
                'readonly' => 1
            ])// field_575d06c631d44
            ->addText('branch_id', [
                'readonly' => 1
            ])// field_57a57e53c15c3
            ->addText('headline')// field_579451188301d
            ->addText('property_type')// field_5738021c707f5
            ->addText('category')// field_575d19a84d6da
            ->addSelect('property_status', [
                'choices' => [
                    ['current' => 'current'],
                    ['sold' => 'sold'],
                    ['leased' => 'leased'],
                    ['withdrawn' => 'withdrawn'],
                    ['offmarket' => 'offmarket'],
                    ['undercontract' => 'undercontract']
                ],
            ])// field_575d1ab083c33
            ->addSelect('property_commercial_listing_type', [
                'choices' => [
                    ['lease' => 'lease'],
                    ['sale' => 'sale'],
                ],
            ])// field_575d1ab083c33
            ->addSelect('address_display', [
                'choices' => [
                    ['yes' => 'yes'],
                    ['no' => 'no'],
                ]
            ])
            ->addText('address_full')// field_5794465801b89
            ->addText('address_sub_number')// field_57c0f2f35e551
            ->addText('address_street_number')// field_574138d878576
            ->addText('address_street')// field_575d2389b3b52
            ->addText('address_suburb')// field_575d23a6b3b53
            ->addText('address_state')// field_575d23e4f01f8
            ->addText('address_postcode')// field_575d2430f01f9
            ->addText('lat')// field_579444616bd70
            ->addText('lon')// field_579444076bd6f
            ->addText('car_spaces')// field_57a55d0ab8431
            ->addSelect('under_offer', [
                'choices' => [
                    ['yes' => 'yes'],
                    ['no' => 'no']
                ]
            ])// field_57d51b1334e73
            ->addText('price')// field_575d293c23468
            ->addSelect('price_display', [
                'choices' => [
                    ['yes' => 'yes'],
                    ['no' => 'no'],
                ]
            ])// field_579d9503c7a18
            ->addText('price_view')// field_579d94eec7a17
            ->addText('rent_amount')// field_57944cb525333
            ->addText('rent_period')
            ->addText('bond_amount')
            ->addText('date_available')
            ->addText('sold_date')
            ->addText('sold_price')
            ->addText('display_sold_price')
            ->addText('auction_date')
            ->addText('created_at', [
                'readonly' => 1
            ])
            ->addText('updated_at', [
                'readonly' => 1
            ])
            ->addRelationship('property_agent', [
                'label'     => 'Agents',
                'post_type' => array(
                    0 => 'agent',
                ),
                'filters'   => array(
                    0 => 'search',
                ),
            ])
            ->addRelationship('office_property', [
                'label'     => 'Office',
                'post_type' => array(
                    0 => 'office',
                ),
                'filters'   => array(
                    0 => 'search',
                ),
                'max'       => 1,
            ])
            ->addRepeater('inspection_times', [
                'layout' => 'table',
            ])
            ->addText('label', ['required' => 1])
            ->addText('timestamp_start', ['required' => 1])
            ->addText('timestamp_end', ['required' => 1]);
    }

    /**
     * @param $fields FieldsBuilder
     */
    private static function imagesTab($fields) {
        $fields
            ->addTab('Images', [
                'placement' => 'left'
            ])
            ->addRepeater('images', [
                'layout'       => 'block',
                'button_label' => 'Add Image',
            ])
            ->addUrl('url');
    }

    /**
     * @param $fields FieldsBuilder
     */
    private static function resourcesTab($fields) {
        $fields
            ->addTab('Resources', [
                'placement' => 'left'
            ])
            ->addUrl('video_link')
            ->addUrl('property_media')
            ->addUrl('floor_plan')
            ->addUrl('3d_floor_plan_image')
            ->addUrl('3d_floor_plan')
            ->addFlexibleContent('resources', [
                'button_label' => 'Add Resource',
            ])
            ->addLayout('link')
            ->addText('label')
            ->addUrl('url')
            ->addLayout('file')
            ->addText('label')
            ->addFile('file')
            ->addLayout('image')
            ->addText('label')
            ->addImage('image');

    }

    /**
     * @param $fields FieldsBuilder
     */
    private static function featuresTab($fields) {
        $fields
            ->addTab('Property Features', [
                'placement' => 'left'
            ])
            ->addText('bedrooms')
            ->addText('bathrooms')
            ->addText('garages')
            ->addText('carports')
            ->addText('open_spaces')
            ->addText('living_areas')
            ->addText('study')
            ->addText('workshop')
            ->addText('pay_tv')
            ->addText('pool_above_ground')
            ->addText('ducted_heating')
            ->addText('air_conditioning')
            ->addText('floorboards')
            ->addText('tennis_court')
            ->addText('built_in_robes')
            ->addText('pool_in_ground')
            ->addText('open_fire_place')
            ->addText('intercom')
            ->addText('ducted_cooling')
            ->addText('dishwasher')
            ->addText('inside_spa')
            ->addText('outside_spa')
            ->addText('land_area')
            ->addText('land_area_unit');
    }
}
