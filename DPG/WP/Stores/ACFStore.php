<?php

namespace DPG\WP\Stores;

use DPG\WP\Contracts\DataStore;
use StoutLogic\AcfBuilder\FieldsBuilder;

abstract class ACFStore implements DataStore {

	/**
	 * @var Integer
	 */
	protected $post_id;

	/**
	 * @var string
	 */
	protected $field_prefix;

	/**
	 * PropertyACFStore constructor.
	 *
	 * @param Integer $post_id
	 */
	public function __construct( $post_id ) {
		$this->post_id = $post_id;
	}

	/**
	 *
	 */
	public function get() {
	}

	/**
	 * Returns single field
	 *
	 * @param $field
	 *
	 * @return mixed|null
	 */
	public function getField( $field ) {
		return get_field( $this->field_prefix . $field, $this->post_id );
	}

	/**
	 * Updates acf field
	 *
	 * @param $field
	 * @param $value
	 */
	public function updateField( $field, $value ) {
		update_field( $this->field_prefix .  $field, $value, $this->post_id );
	}


	public static function addAcfGroup() {
		$fields = static::buildFields();

		acf_add_local_field_group( $fields->build() );
	}

	/**
	 * @return FieldsBuilder
	 */
	abstract protected static function buildFields();
}