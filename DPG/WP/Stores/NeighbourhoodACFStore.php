<?php

namespace DPG\WP\Stores;

use DPG\WP\Models\Neighbourhood;
use StoutLogic\AcfBuilder\FieldsBuilder;

class NeighbourhoodACFStore extends ACFStore {
	/**
	 * @var string
	 */
	protected $field_prefix = 'field_neighbourhood_';

	/**
	 * Saves agent data
	 *
	 * @param $api_property
	 */
	function update( $api_property ) {
		$this->updateField( 'id', Neighbourhood::getUniqueId( $api_property ) );
		$this->updateField( 'postcode', $api_property->address_postcode );
		$this->updateField( 'state', $api_property->address_state );
	}

	/**
	 * @return FieldsBuilder
	 */
	protected static function buildFields() {
		$fields = new FieldsBuilder( 'neighbourhood' );

		self::generalTab( $fields );
		self::introContentTab( $fields );
		self::discoverSuburbTab( $fields );
		self::propertiesTab( $fields );

		$fields->setLocation( 'post_type', '==', 'neighbourhood' );

		return $fields;
	}

	/**
	 * @param $fields FieldsBuilder
	 */
	private static function generalTab( $fields ) {
		$fields
			->addTab( 'General', [
				'placement' => 'left'
			] )
			->addText( 'id', [
				'readonly' => 1
			] )
			->addImage( 'thumbnail_image', [
				'library' => 'uploadedTo'
			] )// field_5778fd8436dbe
			->addText( 'postcode' )// field_57594f572f357
			->addText( 'state' )// field_57594ff02f359
			->addText( 'social_eazie_widget_id' ); // social_eazie_widget_id
	}

	/**
	 * @param $fields FieldsBuilder
	 */
	private static function introContentTab( $fields ) {
		$fields
			->addTab( 'Intro Content', [
				'placement' => 'left'
			] )
			->addWysiwyg( 'lifestyle_intro_content' )// field_5774f888f00ac
			->addWysiwyg( 'people_intro_content' )// field_5774f8aef00ad
			->addWysiwyg( 'homes_intro_content' ); // field_5774f8b1f00ae
	}

	/**
	 * @param $fields FieldsBuilder
	 */
	private static function discoverSuburbTab( $fields ) {
		$videoLayout = new FieldsBuilder( 'video' );
		$videoLayout
			->addRadio( 'video_type', [
				'required'      => 1,
				'wrapper'       => [ 'width' => '50' ],
				'choices'       => array(
					'youtube' => 'YouTube',
					'vimeo'   => 'Vimeo',
				),
				'default_value' => 'youtube',
				'layout'        => 'horizontal',
			] )
			->addText( 'video_id', [
				'required' => 1,
				'wrapper'  => [ 'width' => '50' ],
			] );

		$imageLayout = new FieldsBuilder( 'image' );
		$imageLayout
			->addText( 'label', [
				'wrapper' => [ 'width' => '50' ],
			] )
			->addImage( 'image', [
				'wrapper' => [ 'width' => '50' ],
				'library' => 'uploadedTo',
			] );


		$fields
			->addTab( 'Discover Suburb', [
				'placement' => 'left'
			] )
			->addRepeater( 'discover_content_group', [
				'layout'       => 'block',
				'button_label' => 'Add Content Group'
			] )// field_577664ff3c533
			->addText( 'title' )
			->addRepeater( 'discover_content_row', [
				'layout'       => 'block',
				'button_label' => 'Add Content Row',
			] )
			->addSelect( 'layout', [
				'required' => 1,
				'choices'  => [
					[ 'one_column' => '1 Column' ],
					[ 'two_columns' => '2 Columns' ],
					[ 'two_columns_66_33' => '2 Columns (66% + 33%)' ],
					[ 'three_columns' => '3 Columns' ],
				],
				'ui'       => 1
			] )
			->addFlexibleContent( 'column_one', [
				'label'    => 'Column 1',
				'required' => 1,
				'max'      => 1,
			] )
			->addLayout( $videoLayout )
			->addLayout( $imageLayout )
			->endFlexibleContent()
			->addFlexibleContent( 'column_two', [
				'label'    => 'Column 2',
				'required' => 1,
				'max'      => 1,
			] )
			->addLayout( $videoLayout )
			->addLayout( $imageLayout )
			->conditional( 'layout', '!=', 'one_column' )
			->endFlexibleContent()
			->addFlexibleContent( 'column_three', [
				'label'    => 'Column 3',
				'required' => 1,
				'max'      => 1,
			] )
			->conditional( 'layout', '==', 'three_columns' )
			->addLayout( $videoLayout )
			->addLayout( $imageLayout )
			->endFlexibleContent()
			->endRepeater();
	}

	/**
	 * @param $fields FieldsBuilder
	 */
	private static function propertiesTab( $fields ) {
		$fields
			->addTab( 'Properties', [
				'placement' => 'left'
			] )
			->addNumber( 'commercial_lease' )// field_57a32918763aa
			->addNumber( 'commercial_sale' )// field_57a3298a763ab
			->addNumber( 'residential_sale' )// field_57a329c4763ac
			->addNumber( 'residential_rent' )// field_57a329ed763ad
			->addNumber( 'sold' )// field_57a32a09763ae
			->addNumber( 'leased' ); // field_57a32a0f763af
	}
}