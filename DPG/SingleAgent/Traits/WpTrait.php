<?php
namespace DPG\SingleAgent\Traits;

use DPG\SingleAgent\Help as Help;

/**
 * A set of helper methods to be used in PostTypeHelpers
 */
trait WpTrait {
    /**
     * ID of the
     * current post.
     * @var integer
     */
    protected static $id;
    /**
     * Parses the ID or gets it from the loop. If $id has changed
     * then the class properties are reset.
     * @param  integer $id
     * @return integer
     */
    public static function the_id( $id = null ) {
        if ( is_int($id) && $id !== 0 ) {
            $id = $id;
        } else {
            $id = get_the_ID();
        }

        self::$id = $id;

        return $id;
    }
}
