<?php
namespace DPG\SingleAgent\Traits;

use DPG\SingleAgent\Help as Help;

/**
 * A set of helper methods to be used in PostTypeHelpers
 */
trait HelperTrait {
    /**
     * Static variable for method chaining.
     * @var mixed
     */
    public static $current_value = [];

    /**
     * A list of groups that hold post custom fields.
     * @var array
     */
    protected static $fieldGroups = [];

    /**
     * Instance for method chaining.
     * @var self
     */
    protected static $_instance = null;

    /**
     * Gets or sets an instance of self to chain methods.
     * @return $this
     */
    public static function pipe() {
        if (self::$_instance === null) {
            $class = get_called_class();
            self::$_instance = new $class;
        }

        return self::$_instance;
    }
    /**
     * Kills the instance at a chain endpoint.
     * @return void
     */
    public static function kill() {
        self::$_instance = null;
    }
    /**
     * Wipes all field group properties' data.
     * @return void
     */
    public static function wipe() {
        foreach(static::$fieldGroups as $group) {
            static::$$group = [];
        }
    }
    /**
     * Sets $val to self::$current_value then returns.
     * @param  mixed $val
     * @return mixed $val
     */
    public static function returns($val) {

        self::$current_value = $val;

        /* If there is an instance set it to $val. */
        if( self::$_instance !== null ) {
            $val = self::$_instance;
        }

        return $val;
    }
    /**
     * Encodes current value to JSON then kills the instance.
     * @return JSON
     */
    public function json() {
        self::kill();
        return json_encode(self::$current_value);
    }
}
