<?php
namespace DPG\SingleAgent\Metabox;

use DPG\SingleAgent\Help as Help;
use DPG\SingleAgent\Metabox\Metabox as Metabox;

/**
 * Product Post Type
 *
 * @package   Product_Post_Type
 */

/**
 * This class defines a custom metabox for adding product downloads.
 *
 * @see https://github.com/WebDevStudios/CMB2/
 *
 * @package Product_Post_Type
 */
class PageHeaderMeta extends Metabox {
    static $post_types = ['post', 'page', 'agent'];
    static $group = 'heading_';

    /**
     * Registers metaboxes for adding content to page headings.
     *
     * @since 0.1.0
     */
    public static function metabox() {
        $prefix = self::$prefix;
        $group = self::$group;

        /**
         * Initiate the metabox
         */
        $cmb = new_cmb2_box( array(
            'id'            => $prefix . 'page_header_meta',
            'title'         => __( 'Page Header', 'cmb2' ),
            'object_types'  => self::$post_types, // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
        ) );

        $cmb->add_field(array(
            'name'       => __( 'Heading', 'cmb2' ),
            'id'         => $prefix . $group . 'title',
            'type'       => 'text',
            // 'desc'        => 'Add an alternative title to display on the page header.',
        ) );

        $cmb->add_field(array(
            'name'       => __( 'Byline', 'cmb2' ),
            'id'         => $prefix . $group . 'byline',
            'type'       => 'text',
            // 'desc'        => 'Add byline text to the page header.',
        ) );

        $cmb->add_field(array(
            'name'       => __( 'Image', 'cmb2' ),
            'id'         => $prefix . $group . 'image',
            'type'       => 'file',
            // 'desc'        => 'Add an alternative image to display on the page header.',
        ) );

        $cmb->add_field(array(
            'name'             => __( 'Image Alignment', 'cmb2' ),
            'id'               => $prefix . $group . 'img_pos',
            'type'             => 'select',
            'desc'             => 'Reposition the image within the header section (horizontal vertical).',
            'show_option_none' => false,
            'default'          => 'center center',
            'options'          => array(
                'left top'      => __( 'Top Left', 'cmb2' ),
                'center top'    => __( 'Top Center', 'cmb2' ),
                'right top'     => __( 'Top Right', 'cmb2' ),
                'left center'   => __( 'Center Left', 'cmb2' ),
                'center center' => __( 'Center Center', 'cmb2' ),
                'right center'  => __( 'Center Right', 'cmb2' ),
                'left bottom'   => __( 'Bottom Left', 'cmb2' ),
                'center bottom' => __( 'Bottom Center', 'cmb2' ),
                'right bottom'  => __( 'Bottom Right', 'cmb2' ),
            ),
        ) );

        $cmb->add_field(array(
            'name'       => __( 'Mobile Image', 'cmb2' ),
            'id'         => $prefix . $group . 'mobile_image',
            'type'       => 'file',
            'desc'       => 'Add a formatted image for mobile devices if required.',
        ) );

        $cmb->add_field(array(
            'name'       => __( 'Button Text', 'cmb2' ),
            'id'         => $prefix . $group . 'btn_text',
            'type'       => 'text',
            // 'desc'        => 'Add alternative text for the page header button.',
        ) );

        $cmb->add_field(array(
            'name'       => __( 'Button Link', 'cmb2' ),
            'id'         => $prefix . $group . 'btn_href',
            'type'       => 'text_url',
            // 'desc'        => 'Add alternative text for the page header button.',
        ) );
    }
    /**
     * Callback function to add actions after a post is saved.
     * @return void
     */
    public static function save() {
        // Add actions here...
    }

    /**
     * Get the page heading custom fields.
     * @param  integer $id
     * @return array
     */
    public static function fields( $id=null ) {
        global $pagename;
        if($pagename) {
            $id = Help::getIdBySlug($pagename);
        }
        if ( ! $id ) {
            $id = get_the_ID();
        }

        $prefix          = self::$prefix;
        $group           = self::$group;
        $title           = get_post_meta( $id, $prefix . $group . 'title', true );
        $text            = get_post_meta( $id, $prefix . $group . 'byline', true );
        $bg_img          = get_post_meta( $id, '_dpg-field_heading_image', true );
        $bg_img_pos      = get_post_meta( $id, $prefix . $group . 'img_pos', true );
        $bg_img_mobile   = get_post_meta( $id, $prefix . $group . 'mobile_image', true );
        $btn_text        = get_post_meta( $id, $prefix . $group . 'btn_text', true );
        $btn_href        = get_post_meta( $id, $prefix . $group . 'btn_href', true );

        if ( ! $bg_img ) {
            $bg_img = false;
        }

        if ($bg_img === false) {
            if( has_post_thumbnail($id) ) {
                $bg_img = get_the_post_thumbnail_url( get_the_ID() );
            } else {
                $bg_img   = get_post_meta( 88, '_dpg-product_heading_image', true );
            }
        }

        return array(
            'id'              => $id,
            'title'           => ( ! empty($title) ? $title : get_the_title($id) ),
            'text'            => ( ! empty($text) ? $text : '' ),
            'bg_img'          => $bg_img,
            'bg_img_pos' => $bg_img_pos,
            'bg_img_mobile'   => $bg_img_mobile,
            'btn_text'        => ( ! empty($btn_text) ? $btn_text : 'Find Out More' ),
            'btn_href'        => ( ! empty($btn_href) ? $btn_href : '' ),
        );

        $id = self::the_id($id);
        $group = self::$group;

        $title        = self::getField($group . 'title', $id);
        $byline       = self::getField($group . 'byline', $id);
        $image        = self::getField($group . 'image', $id);
        $mobile_image = self::getField($group . 'mobile_image', $id);
        $btn_text     = self::getField($group . 'btn_text', $id);
        $btn_href     = self::getField($group . 'btn_href', $id);

        $fields = array(
            'title'         => ( ! empty($title) ? $title : get_the_title() ),
            'text'          => ( ! empty($text) ? $text : '' ),
            'bg_img'        => ( ! empty($image) ? $image : get_the_post_thumbnail_url() ),
            'bg_img_mobile' => ( ! empty($mobile_image) ? $mobile_image : get_the_post_thumbnail_url() ),
            'btn_text'      => ( ! empty($btn_text) ? $btn_text : 'Find Out More' ),
            'btn_href'      => ( ! empty($btn_href) ? $btn_href : '' ),
        );

        return $fields;
    }
}
