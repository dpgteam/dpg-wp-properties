<?php
namespace DPG\SingleAgent\Metabox;

use DPG\SingleAgent\Help as Help;
use DPG\SingleAgent\Traits\{HelperTrait, WpTrait};

/**
 * Product Post Type
 *
 * @package   Product_Post_Type
 */

/**
 * This class defines a custom metabox for adding product downloads.
 *
 * @see https://github.com/WebDevStudios/CMB2/
 *
 * @package Product_Post_Type
 */
class Metabox {

    use HelperTrait;
    use WpTrait;

    const PREFIX = '_dpg-field_';
    static $prefix = self::PREFIX;

    /**
     * Registers the metabox and adds save hook.
     * @return void
     */
    public static function init() {
        add_action( 'cmb2_admin_init', array( get_called_class(), 'metabox' ) );
        add_action( 'save_post', array( get_called_class(), 'save' ) );
    }

    /**
     * Gets country terms and displays them as options
     * @param  CMB2_Field $field
     * @return array An array of options that matches the CMB2 options array
     */
    public static function get_country_term_options( $field ) {
        $terms = get_terms( 'countries', array(
            'hide_empty' => false,
        ) );
        // Initate an empty array
        $term_options = array();
        if ( ! empty( $terms ) ) {
            foreach ( $terms as $term ) {
            $term_options[ $term->term_id ] = $term->name;
            }
        }

        return $term_options;
    }
    public static function getField($name, $id=null) {
        $id = self::the_id($id);
        return get_post_meta( $id, self::$prefix . $name, true );
    }
}
