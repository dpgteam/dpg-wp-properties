<?php
namespace DPG\SingleAgent;
/**
 * The Icon class contains a set of static methods for outputting
 * markup for generating site icons here.
 */
use DPG\SingleAgent\Traits\WpTrait as WpTrait;

/**
 * Outputs site icons.
 */
class Icon {
    /**
     * Base HTML for generating icons.
     * @param  string $icon
     * @param  string $modifier
     * @return output
     */
    public static function htmlIcon($icon, $modifier = 'dark') {

        return '<i class="icon-' . $icon . ' ' . $modifier . '"></i>';
    }
    public static function bathrooms($modifier = 'dark') {
        $icon = self::htmlIcon('bathrooms', $modifier);
        echo $icon;

        return $icon;
    }
    public static function bedrooms($modifier = 'dark') {
        $icon = self::htmlIcon('bedrooms', $modifier);
        echo $icon;
        return $icon;
    }
    public static function garage($modifier = 'dark') {
        $icon = self::htmlIcon('garage', $modifier);
        echo $icon;

        return $icon;
    }
    public static function mail($modififier = 'dark') {
        echo '<i class="small mail outline icon"></i>';
    }
    public static function mobile($modififier = 'dark') {
        echo '<i class="mobile icon"></i>';
    }
    public static function phone($modififier = 'dark') {
        echo '<i class="small call icon"></i>';
    }
    public static function avatarUrl() {
        echo self::getAvatarUrl();
    }
    public static function getAvatarUrl() {
        return '/app/themes/bigginscott/dist/images/person-avatar.png';
    }
}
