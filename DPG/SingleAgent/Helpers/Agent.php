<?php

namespace DPG\SingleAgent\Helpers;

use DPG\WP\Admin\Settings;
use WP_Query;
use DPG\SingleAgent\Help as Help;
use DPG\SingleAgent\Helpers\PostTypeHelper as PostTypeHelper;

/**
 * A collection of static helper methods for retrieving office post content.
 */
class Agent extends PostTypeHelper
{
    protected $post_type = 'agent';
    protected $slug = '';
    protected $postData = null;
    protected $currentProperties = null;
    protected $soldProperties = null;
    protected $suburbs = null;
    protected $neighbourhoods = null;

    public function __construct($slug = null)
    {
        $slug = $slug ?? get_field('agent_slug', 'option');
        if ($slug !== null) {
            $this->get($slug);
        } else if (is_int($slug)) {
            parent::__construct($slug);
        }
    }

    /**
     * Gets the default agent or agent having $slug if passed.
     * @param  string|integer $slug The agent slug or post ID.
     * @return WP_Post
     */
    public function get($slug = null)
    {
        $this->slug = $slug ?? get_field('agent_slug', 'option') ?? get_option('wp_dpg_importer_options')['agent_slug'];
        $args = [
            'numberposts' => 1,
            'post_type' => 'agent',
        ];
        if (is_string($this->slug)) {
            $args['name'] = $this->slug;
        } else if (is_int($this->slug)) {
            $args['p'] = $this->slug;
        }
        $this->postData = get_posts($args)[0] ?? null;
        if ($this->postData) {
            $this->postData->fields = get_fields($this->postData->ID);
        }
        return $this->postData;
    }

    public function featuredImage()
    {
        // var_dump($this->postData->fields['local_video']); die;
        if (
            is_front_page() &&
            (!Help::mobile() && !Help::tablet()) &&
            (!empty($this->postData->fields['local_video']) && !empty($this->postData->fields['video_id']))
        ) {
            $data = [
                'video_id' => $this->postData->fields['video_id'],
                'video_type' => $this->postData->fields['video_type'],
                'local_video' => $this->postData->fields['local_video'],
                'featured_image' => count($this->featured_image()) ? $this->featured_image() : $this->image()->url . '?v=imported'
            ];
        } else if (count($this->featured_image())) {
            $data = $this->featured_image();
        } else {
            $data = $this->image()->url . '?v=imported';
        }
        return $data;
    }

    public function image()
    {
        return $this->postData->fields['photo'] ?? $this->defaultAgentImage();
    }

    public function featured_image()
    {
        $featured = Help::makeFeaturedImageArray($this->postData->ID);
        if (count($featured))
            return $featured;
        return $this->defaultAgentImageArray();
    }

    public function phone()
    {
        return $this->postData->fields['phone'] ?? null;
    }

    public function mobile()
    {
        return $this->postData->fields['mobile'] ?? null;
    }

    public function email()
    {
        return $this->postData->fields['email'] ?? null;
    }

    public function role()
    {
        return $this->postData->fields['title'] ?? null;
    }

    public function social()
    {
        return $this->postData->fields['social_media'] ?? null;
    }

    public function socialEazie()
    {
        return $this->postData->fields['social_eazie_widget_id'] ?? null;
    }

    public function offices()
    {
        return $this->postData->fields['offices'] ?? [];
    }

    public function trustpilot()
    {
        return $this->postData->fields['trust_pilot_tags'] ?? null;
    }

    public function defaultAgentImage()
    {
        return Settings::get_option('default_agent_image', 'frontend');
    }

    public function defaultAgentImageArray()
    {
        $image = Settings::get_option('default_agent_image', 'frontend');
        $images = [];
        if ($image) {

            $images['full'] = [
                'size' => 'full',
                'width' => 350,
                'height' => 350,
                'url' => $image,
            ];
        }
        return array_values($images);
    }

    public function testimonials()
    {
        $testimonials = is_array($this->postData->fields['testimonials']) ? $this->postData->fields['testimonials'] : [];
        if (count($testimonials)) {
            foreach ($testimonials as $key => $testimonial) {
                $testimonials[$key]['details'] = str_replace('<br />', '</p><p>', $testimonial['details']);
            }
        }
        return $testimonials;
    }

    public function videos()
    {
        return array_merge([[
            'video_type' => $this->postData->fields['video_type'],
            'video_id' => $this->postData->fields['video_id'],
        ]],
            $this->postData->fields['additional_videos']
        );
    }

    public function neighbourhoods()
    {
        if (!$this->neighbourhoods) {
            $neighbourhoods = new WP_Query([
                'post_type' => 'neighbourhood',
                'posts_per_page' => -1,
                'order' => 'ASC',
                'orderby' => 'title',
            ]);
            foreach ($neighbourhoods->posts as $key => $post) {
                if (!in_array($post->post_title, $this->suburbs())) {
                    unset($neighbourhoods->posts[$key]);
                }
            }
            $this->neighbourhoods = $neighbourhoods->posts;
        }
        return $this->neighbourhoods;
    }

    /**
     * Check an agent's properties and returns a list of unique suburb names.
     * @return array
     */
    public function suburbs()
    {
        if (!$this->suburbs) {
            foreach ([$this->current(), $this->sold()] as $properties) {
                foreach ($properties as $property) {
                    $this->suburbs[] = $property->fields['address_szuburb'];
                }
            }
            sort($this->suburbs);
            $this->suburbs = array_values(array_unique($this->suburbs));
        }
        return $this->suburbs;
    }

    public function current()
    {
        if (!$this->currentProperties) {
            $this->currentProperties = $this->getProperties([
                'key' => 'property_status',
                'value' => 'current'
            ]);
        }
        return $this->currentProperties ?? null;
    }

    public function sold()
    {
        if (!$this->soldProperties) {
            $this->soldProperties = $this->getProperties([
                'key' => 'property_status',
                'value' => 'sold'
            ], true);
        }
        return $this->soldProperties ?? null;
    }

    /**
     * Returns an array of Property posts with custom field data attached.
     * @param  Array $meta_args
     * @param  bool $sold
     * @return Array
     */
    protected function getProperties($meta_args, $sold = false)
    {
        $args = $this->defaultPropertyArgs($sold);
        $args['meta_query'][1] = $meta_args;
        $properties = new WP_Query($args);
        $properties = $properties->posts;
        foreach ($properties as $key => $property) {
            $properties[$key]->fields = get_fields($property->ID);
        }
        return $properties;
    }

    /**
     * Default WP_Query arguments for getting this Agent's properties.
     * @return Array
     */
    protected function defaultPropertyArgs($sold)
    {
        return [
            'post_type' => 'property',
            'posts_per_page' => -1,
            'order' => 'DESC',
            'orderby' => 'meta_value',
            'meta_key' => $sold ? 'updated_at' : 'created_at',
            'meta_type' => 'DATETIME',
            'meta_query' => array(
                [
                    'key' => 'property_agent',
                    'value' => '"' . $this->id() . '"',
                    'compare' => 'LIKE'
                ],
            )
        ];
    }
}
