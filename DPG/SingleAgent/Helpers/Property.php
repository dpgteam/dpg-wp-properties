<?php

namespace DPG\SingleAgent\Helpers;

use WP_Query;

use DPG\SingleAgent\Help as Help;
use DPG\SingleAgent\Helpers\PostTypeHelper as PostTypeHelper;

/**
 * A collection of static helper methods for retrieving property post content.
 */
class Property extends PostTypeHelper
{
    protected $post_type = 'property';
    protected static $fieldGroups = [
        'address',
        'agents',
        'features',
        'inspections',
        'media',
        'pricing',
    ];

    protected $address = [];
    protected $agents = [];
    protected $features = [];
    protected $general = [];
    protected $inspections = [];
    protected $media = [];
    protected $pricing = [];

    public function __construct($data = null) {
        if (is_object($data)) {
            parent::__construct($data);
        } else {
            $this->getProperty($data ?? get_the_ID());
        }
    }

    public function id() {
        return $this->postData->ID;
    }

    public function fields() {
        return $this->postData->fields;
    }

    /**
     * Returns an associative array of property address custom fields.
     *
     * @param integer $id
     *
     * @return array
     */
    public function address() {

        if ( ! count($this->address)) {
            /* Build street address from custom fields. */
            $address['street'] = '';
            if ($sub = $this->postData->fields['address_sub_number'] ?? '') {
                $address['street'] .= $sub . '/';
            }
            $address['street'] .= $this->postData->fields['address_street_number'] . ' ';
            $address['street'] .= $this->postData->fields['address_street'];

            /* Populate array with remaining fields */
            $address['suburb']   = $this->postData->fields['address_suburb'];
            $address['state']    = $this->postData->fields['address_state'];
            $address['postcode'] = $this->postData->fields['address_postcode'];
            $address['country']  = 'Australia';

            /* Map Coordinates */
            $address['location'] = array(
                'lat' => $this->postData->fields['lat'],
                'lon' => $this->postData->fields['lon'],
            );

            $this->address = $address;
        }

        return $this->address;
    }

    public function addressStr() {
        $address = $this->address();

        return "{$address['street']}, {$address['suburb']} {$address['postcode']}";
    }

    /**
     * Returns the property's agents in an array.
     *
     * @param integer $id
     *
     * @return array
     */
    public function agents() {
        return $this->postData->fields['property_agent'];
    }

    /**
     * Returns an associative array of property feature custom fields.
     *
     * @param integer $id
     *
     * @return array
     */
    public function features() {
        return [
            'bathrooms'  => $this->postData->fields['bathrooms'],
            'bedrooms'   => $this->postData->fields['bedrooms'],
            'car_spaces' => $this->postData->fields['car_spaces'],
        ];
    }

    /**
     * Returns and associative array of custom fields
     * for property inspection and auction times.
     *
     * @param integer $id
     *
     * @return array
     */
    public function inspections() {
        $fields  = [];
        $auction = $this->postData->fields['auction_date'] ?? null;
        if (strtotime($auction) > time()) {
            $fields['auction_date'] = $auction;
        } else {
            $fields['auction_date']           = null;
            $fields['auction_date_formatted'] = date('D j M: g:ia', strtotime($fields['auction_date']));
        }

        $fields['inspection_times'] = $this->postData->fields['inspection_times'];
        /**
         *  Remove inspection times that have already passed
         *  and add formatted string for future dates.
         */
        if ( ! empty($fields['inspection_times'])) :
            foreach ($fields['inspection_times'] as $key => $inspection_time) :
                if ($inspection_time['timestamp_end'] <= time()) {

                    unset($fields['inspection_times'][$key]);

                } else {

                    $time       = $fields['inspection_times'][$key];
                    $date_start = date('D j M: g:ia', $time['timestamp_start']);
                    $date_end   = date('g:ia', $time['timestamp_end']);

                    $fields['inspection_times'][$key]['formatted'] = "{$date_start} - {$date_end}";

                }
            endforeach;
        endif;
        /* Cast null value to empty array. */
        if ($fields['inspection_times'] === null) {
            $fields['inspection_times'] = [];
        }
        /* Sort remaining inspections by start time. */
        if (is_array($fields['inspection_times'])) {
            usort($fields['inspection_times'], function ($a, $b) {
                return $a['timestamp_start'] <=> $b['timestamp_start'];
            });
        }


        return $fields;
    }

    /**
     * Returns an associative array of property media custom fields.
     *
     * @param integer $id
     *
     * @return array
     */
    public function media() {
        $fields['plan']                = $this->postData->fields['floor_plan'] ?? null;
        $fields['3d_floor_plan_image'] = $this->postData->fields['3d_floor_plan_image'] ?? null;
        $fields['video']               = $this->postData->fields['video_link'] ?? null;
        $fields['images']              = $this->postData->fields['images'] ?? null;
        $fields['property_media']      = $this->postData->fields['property_media'] ?? null;
        $fields['3d_floor_plan']       = $this->postData->fields['3d_floor_plan'] ?? null;

        return $fields;
    }

    /**
     * Returns an associative array of property pricing custom fields.
     *
     * @param integer $id
     *
     * @return array
     */
    public function pricing() {
        $fields = [
            'property_status'    => $this->postData->fields['property_status'] ?? null,
            'price'              => $this->postData->fields['price'] ?? null,
            'price_display'      => $this->postData->fields['price_display'] ?? null,
            'price_view'         => $this->postData->fields['price_view'] ?? null,
            'rent_amount'        => $this->postData->fields['rent_amount'] ?? null,
            'rent_period'        => $this->postData->fields['rent_period'] ?? null,
            'sold_price'         => $this->postData->fields['sold_price'] ?? null,
            'display_sold_price' => $this->postData->fields['display_sold_price'] ?? null,
        ];

        $fields['price_display'] = $fields['price_display'] == 'yes' ? true : false;

        return $fields;
    }

    /**
     * Returns the property's property type.
     * @return string
     */
    public function type() {
        return $this->postData->fields['property_type'];
    }

    /**
     * Returns the property's property status.
     * @return string
     */
    public function status() {
        return $this->postData->fields['property_status'];
    }

    /**
     * Gets the property's created_at value.
     * @return string
     */
    public function created() {
        return $this->postData->fields['created_at'];
    }

    /**
     * Gets the property's bond_amount value.
     * @return string
     */
    public function bond_amount() {
        return $this->postData->fields['bond_amount'] ?? null;
    }

    /** Gets the property unique id
     * @return null
     */
    public function unique_id() {
        return str_replace($this->postData->fields['branch_id'] . '-', '', $this->postData->fields['id']);
    }

    /**
     * Gets the property's date_available value.
     * @return string
     */
    public function date_available() {
        if ( ! empty($this->postData->fields['date_available'])) {
            $date = strtotime($this->postData->fields['date_available']);
            $date = date('l d F Y', $date);
        }

        return $date ?? null;
    }

    /**
     * Returns true if this property's type is a commercial one.
     *
     * @param integer $id
     *
     * @return boolean
     */
    public function commercial() {
        return in_array($this->type(), ['commercial_lease', 'commercial_sale']);
    }

    /**
     * Returns true if this property is listed as being under offer.
     * @return boolean
     */
    public function offer() {
        return $this->postData->fields['under_offer'] == 'yes';
    }

    /**
     * Returns true if the property's status is sold or leased.
     *
     * @param integer $id
     *
     * @return boolean
     */
    public function soldOrLeased() {
        return in_array($this->postData->fields['property_status'], ['sold', 'leased']);
    }

    /**
     * Returns HTML for display property status for
     * new, leased, sold or under offer properties.
     *
     * @param integer $id
     *
     * @return string
     */
    public function statusTab() {
        $status = $this->determinePropertyStatus();

        if ($status) {
            $status_tab = "<span class=\"{$status}\">" . ucwords(str_replace('-', ' ', $status)) . "</span>";
        }

        return $status_tab ?? '';
    }

    /**
     * Determines the property's display status (new, sold, leased etc).
     *
     * @param integer $id
     *
     * @return string
     */
    public function determinePropertyStatus() {
        // Check the property status and set the appropriate tab
        switch (true) {
            case ($this->status() == 'sold'):
                $status = 'sold';
                break;
            case ($this->status() == 'leased') :
                $status = 'leased';
                break;
            case ($this->offer() && ! $this->soldOrLeased()) :
                $status = 'under-offer';
                break;
            case (strtotime($this->created()) - 604800 - 36000) > time(): // - 7 days - 10h as api dates are in Austrialia/Melbourne
                $status = 'new';
                break;
            default:
                $status = '';
        }

        return $status;
    }

    public function determinePropertyType() {
        if ($this->type() != 'commercial') {
            return $this->type();
        }
        $listing_type = $this->postData->fields['property_commercial_listing_type'] ?? 'sale';
        if ($listing_type != 'sale') {
            return 'commercial_lease';
        }

        return 'commercial_sale';
    }


    /**
     * Checks property pricing custom fields and
     * determines the correct price to display.
     *
     * @param integer $id
     *
     * @return string
     */
    public function displayPrice() {
        $pricing = $this->pricing();
        extract($pricing);
        $price = '';

        /* If property is current or leased. */
        if (in_array($property_status, ['current', 'leased'])) {

            /* Set correct price string. */
            if ($price_display == 'yes' && $price_view) {

                $price = $price_view;

            } else if ( ! empty($rent_amount) && empty($price)) {

                $price = '$' . $rent_amount . ($rent_period === 'annual' ? ' per annum' : ' per week');

            } elseif ($price_display) {

                $price = '$' . number_format($price);

            }

        } elseif ($property_status === 'sold') {

            if ($display_sold_price === 'yes') {

                $price = '$' . number_format($sold_price);

            } else if ($price_view) {

                $price = $price_view;
            }

        }

        if (empty($price)) {
            $price = 'Contact Agent';
        }

        return $price;
    }

    /**
     * Returns the property Floor Plan link.
     *
     * @param integer $id ]
     *
     * @return string
     */
    public function floorPlan() {
        return $this->media()['plan'] ?? null;
    }

    /**
     * Returns the property image links.
     *
     * @param integer $id ]
     *
     * @return array
     */
    public function images() {
        return $this->media()['images'] ?? null;
    }

    /**
     * Returns the property Video link.
     *
     * @param integer $id ]
     *
     * @return string
     */
    public function video() {
        return $this->media['video_link'] ?? null;
    }

    /**
     * Returns an array of Property posts with custom field data attached.
     *
     * @param Array $meta_args
     *
     * @return Array
     */
    protected function getProperty($id) {
        $args     = [
            'p'              => $id,
            'post_type'      => 'property',
            'posts_per_page' => -1,
            'order'          => 'DESC',
            'orderby'        => 'post_modified',
        ];
        $property = new WP_Query($args);
        $property = $property->posts;
        foreach ($property as $key => $prop) {
            $property[$key]->fields = get_fields($prop->ID);
        }
        $this->postData = $property[0];

        return $property;
    }
}
