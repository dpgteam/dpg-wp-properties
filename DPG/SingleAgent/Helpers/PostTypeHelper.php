<?php
namespace DPG\SingleAgent\Helpers;

use DPG\SingleAgent\Help as Help;
use DPG\SingleAgent\Traits\{HelperTrait, WpTrait};
/**
 * Base class for post type helpers.
 * Make sure to set the $post_type in the child class
 * so that the correct post data is retrieved.
 */
class PostTypeHelper {
    use HelperTrait;
    use WpTrait;
    /**
     * The post type for this helper.
     * @var string
     */
    protected $post_type = 'post';
    /**
     * WP Post Data with Custom Fields attached.
     * @var null
     */
    protected $postData = null;
    /**
     * If existing post data is passed this is set.
     * If an integer is passed, the post id matching that data is set.
     * If no id is passed then get_the_ID() is used.
     * @param void
     */
    public function __construct($data = null) {
        if ( is_int($data) || $data === null  ) {
            // Get the post data
            $this->get_post($data ?? \get_the_ID());
        } else {
            $this->postData = $data;
        }
        if( ! $this->postData->fields ?? null ) {
            $this->postData->fields = \get_fields($this->postData->ID);
        }
    }
    /**
     * Gets the post WP_Post data for post with $id.
     * @param  integer $id
     * @return void
     */
    protected function get_post($id = null) {
        $args = [
            'p'           =>  $id ?? \get_the_ID(),
            'numberposts' => 1,
            'post_type'   => $this->post_type,
        ];
        $this->postData = \get_posts($args)[0] ?? null;
    }
    /**
     * Gets the custom fields for the post.
     * @return array
     */
    protected function get_fields() {
        return \get_fields($this->postData->ID ?? \get_the_ID());
    }
    /**
     * The post ID.
     * @return integer
     */
    public function id() {
        return $this->postData->ID;
    }
    /**
     * The post title.
     * @return string
     */
    public function title() {
        return $this->postData->post_title;
    }
	public function slug() {
		return $this->postData->post_name;
	}
    public function href() {
        return \get_the_permalink($this->postData->ID);;
    }
    /**
     * Get the post content.
     * @return string
     */
    public function get_content() {
        return \wpautop($this->postData->post_content);
    }
    /**
     * Echo the post content.
     * @return void
     */
    public function content() {
        echo $this->get_content();
    }
    /**
     * Recursively casts $obj StdClass to Array
     * @param  StdClass $obj
     * @return array
     */
    function object_to_array($obj) {
        if(is_object($obj)) $obj = (array) $obj;
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = $this->object_to_array($val);
            }
        }
        else $new = $obj;
        return $new;
    }
}
