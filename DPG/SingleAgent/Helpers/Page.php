<?php
namespace DPG\SingleAgent\Helpers;

use DPG\SingleAgent\Traits\WpTrait as WpTrait;

use DPG\SingleAgent\Help as Help;
use DPG\SingleAgent\Helpers\PostTypeHelper as PostTypeHelper;
use DPG\SingleAgent\Metabox\PageHeaderMeta as PageHeader;

/**
 * A collection of static helper methods for retrieving page content.
 */
class Page extends PostTypeHelper {
    use WpTrait;
    /**
     * Gets custom header fields or falls back to defaults.
     * @param  integer $id
     * @return array
     */
    public static function header($id=null) {
        $id = self::the_id($id);

        return PageHeader::fields($id);
    }
    /**
     * Returns the path to dist assets.
     * @return string
     */
    public static function dist() {

        return get_template_directory_uri() . '/dist/';
    }
    public static function siteSocial() {
        return array(
            'facebook'  => get_theme_mod('sitewide_facebook'),
            'twitter'   => get_theme_mod('sitewide_twitter'),
            'instagram' => get_theme_mod('sitewide_instagram'),
            'linkedin'  => get_theme_mod('sitewide_linkedin'),
            'youtube'   => get_theme_mod('sitewide_youtube'),
        );
    }
}
