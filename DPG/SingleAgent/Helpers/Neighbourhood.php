<?php

namespace DPG\SingleAgent\Helpers;

use WP_Query;
use DPG\SingleAgent\Help as Help;
use DPG\SingleAgent\Helpers\PostTypeHelper as PostTypeHelper;
use DPG\SingleAgent\Metabox\PageHeaderMeta as PageHeader;

/**
 * A collection of static helper methods for retrieving neighbourhood post content.
 */
class Neighbourhood extends PostTypeHelper {
	protected $post_type = 'neighbourhood';

	/**
	 * The postcode custom field data.
	 * @return array
	 */
	public function postcode() {
		return $this->postData->fields['postcode'];
	}

    /**
     * The postcode custom field data.
     * @return array
     */
    public function state() {
        return $this->postData->fields['state'];
    }

	/**
	 * The image custom field data.
	 * @return array
	 */
	public function image() {
		return $this->postData->fields['image'] ?? null;
	}

	/**
	 * The image custom field data.
	 * @return array
	 */
	public function featured_image() {
		return Help::makeFeaturedImageArray( $this->postData->ID );
	}

	/**
	 * The discover custom field data.
	 * @return array
	 */
	public function discover() {
		$discover = $this->postData->fields['discover_content_group'] ?? [];

		return is_array( $discover ) ? $discover : [];
	}
}
