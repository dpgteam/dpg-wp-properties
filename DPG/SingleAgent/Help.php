<?php

namespace DPG\SingleAgent;

use WP_Query;

use DPG\SingleAgent\Traits\WpTrait as WpTrait;

global $mobileDetect;
$mobileDetect = new \Mobile_Detect;

/**
 * A set of helper methods which can be used for rendering page templates.
 */
class Help
{

    use WpTrait;

    /**
     * The location of template partials, relative
     * to the project's root directory.
     */
    const PARTIAL_DIR = '/templates/partials/';

    /**
     * Takes a phone number string strips spaces and special characters.
     *
     * @param  string $phone
     *
     * @return string
     */
    public static function stripPhone($phone)
    {
        $phone = trim($phone);
        $phone = str_replace(' ', '', $phone);
        $phone = str_replace('-', '', $phone);
        $phone = str_replace('(', '', $phone);
        $phone = str_replace(')', '', $phone);

        return $phone;
    }

    /**
     * Checks for mobile device and returns the appropriate
     * tab element name for tab / accordion display.
     * @return string
     */
    public static function tabElement()
    {
        if (self::notMobile()) {
            $tabElement = 'tab-container';
        } else {
            $tabElement = 'accordion-container';
        }

        echo $tabElement;
    }


    /**
     * Formats a mobile phone number for display.
     *
     * @param  mixed $phone The phone number string.
     *
     * @return string
     */
    public static function mobileFormat($phone)
    {
        $phone = self::stripPhone($phone);
        $phone = substr($phone, 0, 4) . ' ' . substr($phone, 4, 3) . ' ' . substr($phone, 7, 3);

        return $phone;
    }

    /**
     * Makes a list of $post thumbnail image sizes with image
     * dimensions and full URL to image file.
     *
     * @param  integer $post
     *
     * @return array
     */
    public static function makeFeaturedImageArray($post_id)
    {
        $id = get_post_thumbnail_id($post_id);
        $meta = wp_get_attachment_metadata($id);
        $images = [];
        if ($meta) :
            $images['full'] = [
                'size' => 'full',
                'width' => $meta['width'],
                'height' => $meta['height'],
                'url' => get_the_post_thumbnail_url($post_id),
            ];
            foreach ($meta['sizes'] as $key => $size) {
                $images[$key] = [
                    'size' => $key,
                    'width' => $size['width'],
                    'height' => $size['height'],
                    'url' => get_the_post_thumbnail_url($post_id, $key),
                ];
            }
        endif;

        // var_dump($images); die;
        return array_values($images);
    }

    /**
     * Returns page ID for $slug
     *
     * @param  string $slug The slug of the page.
     *
     * @return integer
     */
    public static function getIdBySlug($slug)
    {
        $id = null;
        $page = get_page_by_path($slug);
        if ($page) {
            $id = $page->ID;
        }

        return $id;
    }

    /**
     * Returns page url from path or id.
     *
     * @param  integer|string $slug The slug or id of the page.
     *
     * @return string
     */
    public static function getPageUrl($slug = null)
    {

        if (is_string($slug)) {
            $slug = self::getIdBySlug($slug);
        }

        return get_permalink($slug);
    }

    /**
     * Returns the URL of the posts page.
     * @return string
     */
    public static function postsPageUrl()
    {
        if ('page' == get_option('show_on_front')) {
            $url = get_permalink(get_option('page_for_posts'));
        } else {
            $url = home_url();
        }

        return $url;
    }

    /**
     * Returns the root directory of the project.
     * @return string
     */
    public static function projectRoot()
    {
        return dirname(DPG_PLUGIN_FILE);
    }

    /**
     * Sluggifies a string.
     *
     * @param  string $slug The term to sluggify.
     *
     * @return string
     */
    public static function makeSlug($slug)
    {
        $slug = preg_replace('~[^\\pL0-9_]+~u', '-', $slug);
        $slug = trim($slug, "-");
        $slug = iconv("utf-8", "us-ascii//TRANSLIT", $slug);
        $slug = strtolower($slug);
        $slug = preg_replace('~[^-a-z0-9_]+~', '', $slug);

        return $slug;
    }

    /**
     * Sluggifies and echoes a string.
     *
     * @param  string $slug The term to sluggify.
     *
     * @return void
     */
    public static function slug($slug)
    {

        echo self::makeSlug($slug);
    }

    /**
     * Echoes CSS background declaration for post featured image.
     * @return void
     */
    public static function featuredImgBG($id = null)
    {
        $id = self::the_id($id);

        if (has_post_thumbnail($id)) {
            echo "background-image:url(" . self::featuredImg($id) . ");";
        }
    }

    /**
     * Returns the featured image URL for the post with $id.
     *
     * @param  integer $id
     *
     * @return string
     */
    public static function featuredImg($id = null)
    {
        $id = $id ?? get_the_ID();

        if (has_post_thumbnail($id)) {
            return get_the_post_thumbnail_url($id);
        }
    }

    /**
     * Gets an image URL from the WordPress database which has $title.
     *
     * @param  string $title
     *
     * @return string
     */
    public static function getImgByTitle($title)
    {

        $attachment = get_page_by_title($title, OBJECT, 'attachment');
        //print_r($attachment);

        if ($attachment) {

            $attachment_url = $attachment->guid;

        }

        return $attachment_url ?? null;
    }

    /**
     * Returns a random Agent featured image URL
     *
     * @param  null $agent_img
     *
     * @return string
     */
    public static function randomAgentImg($agent_img = null)
    {
        $args = [
            'post_type' => 'agent',
            'posts_per_page' => 1,
            'orderby' => 'rand',
            'meta_query' => array(
                [
                    'key' => '_thumbnail_id',
                    'compare' => 'EXISTS'
                ],
            )
        ];

        $agent_img = new WP_Query($args);
        if (count($agent_img->posts)) {
            $agent_img = Help::featuredImg($agent_img->posts[0]->ID);
        } else {
            $agent_img = '';
        }

        return $agent_img;
    }

    /**
     * Returns a random Agent featured image URL
     *
     * @param  null $agent_img
     *
     * @return string
     */
    public static function randomAuctionImg()
    {
        $meta_args = array(
            [
                'key' => 'property_status',
                'compare' => '=',
                'value' => 'current'
            ],
            [
                'key' => 'auction_date',
                'value' => date('Y-m-d'),
                'compare' => '>=',
                'type' => 'DATE',
            ],
        );
        $img = self::randomPropertyImg($meta_args);

        return $img;
    }

    /**
     * Returns a random Agent featured image URL
     *
     * @param  null $agent_img
     *
     * @return string
     */
    public static function randomInspectionImg($sale = true)
    {

        if ($sale) {
            $price_key = 'price';
        } else {
            $price_key = 'rent_amount';
        }

        $meta_args = array(
            [
                'key' => 'property_status',
                'compare' => '=',
                'value' => 'current'
            ],
            [
                'key' => $price_key,
                'value' => 0,
                'compare' => '>'
            ]
        );

        $args[2] = ['relation' => 'OR'];
        for ($i = 0; $i <= 5; $i++) {
            $args[2][] = [
                'key' => "inspection_times_{$i}_timestamp_end",
                'value' => time() - 43200,    // t - 12h
                'compare' => '>='
            ];
        }

        $img = self::randomPropertyImg($meta_args);

        return $img;
    }

    public static function randomPropertyImg($meta_args = null)
    {
        $img = '';
        $args = [
            'post_type' => 'property',
            'posts_per_page' => 1,
            'orderby' => 'rand',
        ];
        if ($meta_args) {
            $args['meta_query'] = $meta_args;
        }

        $property = new WP_Query($args);
        if (count($property->posts)) {
            $img = get_field('images', $property->posts[0]->ID)[0]['url'];
        }

        return $img;
    }

    /**
     * Returns a random Office featured image URL.
     *
     * @param  null $office_img
     *
     * @return string
     */
    public static function randomOfficeImg($office_img = null)
    {
        $args = [
            'post_type' => 'office',
            'posts_per_page' => 1,
            'orderby' => 'rand',
            'meta_query' => array(
                [
                    'key' => '_thumbnail_id',
                    'compare' => 'EXISTS'
                ]
            )
        ];

        $office_img = new WP_Query($args);
        if (count($office_img->posts)) {
            $office_img = Help::featuredImg($office_img->posts[0]->ID);
        } else {
            $office_img = '';
        }

        return $office_img;
    }

    /**
     * Wrapper for MobileDetect::isMobile()
     * @return boolean
     */
    public static function isMobile()
    {
        global $mobileDetect;

        return $mobileDetect->isMobile();
    }

    /**
     * Wrapper for MobileDetect::isTable()
     * @return boolean
     */
    public static function isTablet()
    {
        global $mobileDetect;

        return $mobileDetect->isTablet();
    }

    /**
     * Returns true if mobile but not tablet.
     * @return boolean
     */
    public static function mobile()
    {

        return (self::isMobile() && !self::isTablet());
    }

    /**
     * Returns true if not mobile but tablet.
     * @return boolean
     */
    public static function notMobile()
    {

        return (!self::isMobile() || self::isTablet());
    }

    /**
     * Returns true if mobile and tablet.
     * @return boolean
     */
    public static function tablet()
    {

        return (self::isMobile() && self::isTablet());
    }

    /**
     * Returns true if not tablet.
     * @return boolean
     */
    public static function notTablet()
    {

        return (!self::isTablet());
    }

    /**
     * Adds an ordinal suffix to a number (1st, 2nd, 3rd or 4th)
     *
     * @param  integer $num
     *
     * @return string
     */
    public static function numSuffix($num)
    {
        if (!in_array(($num % 100), array(11, 12, 13))) {
            switch ($num % 10) {
                // Handle 1st, 2nd, 3rd
                case 1:
                    return $num . 'st';
                case 2:
                    return $num . 'nd';
                case 3:
                    return $num . 'rd';
            }
        }

        return $num . 'th';
    }

    /**
     * Human readable date format.
     *
     * @param  string $datestring
     *
     * @return string
     */
    public static function dateFormat($datestring)
    {
        $date = strtotime($datestring);

        return self::numSuffix(date('d', $date)) . ' ' . date('F, Y', $date);
    }

    /**
     * Includes a template from the partials folder with $items as data.
     *
     * @param  string $name The partial name (foo.bar = ./partials/foo/bar)
     * @param  array $items An array of data for use in template.
     *
     * @return void|string
     */
    public static function getPartial($name = '', $items = [], $path = false)
    {
        if (!$name) {
            return;
        } else {
            $name = str_replace('.php', '', $name);
            $name = str_replace('.', '/', $name);
        }
        if (!empty($items)) {
            extract($items);
        }

        $dir = self::projectRoot() . '/templates/partials/';
        $file_path = $dir . $name . '.php';
        if ($path) {
            return $file_path;
        } else {
            include($file_path);
        }
    }

    /**
     * Shorthand for getting a partial.
     *
     * @param  string $name The partial name (foo.bar = ./partials/foo/bar)
     * @param  array $items An array of data for use in template.
     *
     * @return void
     */
    public static function partial($name = '', $items = [])
    {

        self::getPartial($name, $items);
    }

    public static function getVueify($data)
    {
        return htmlspecialchars(json_encode($data));
    }

    public static function vueify($data)
    {
        echo self::getVueify($data);
    }

    public static function getTab()
    {
        return self::notMobile() ? 'tab' : 'accordion';
    }

    public static function tab()
    {
        echo self::getTab();
    }

    /**
     * @param $video_link
     *
     * @return array
     */
    public static function getVideoData($video_link)
    {
        $video_id = strrchr($video_link, '/');
        $video_id = str_replace('/', '', $video_id);

        if (strpos($video_link, 'vimeo') !== false) {
            $video_type = 'vimeo';
        } else if (strpos($video_link, 'youtu.be') !== false) {
            $video_type = 'youtube';
        } else {
            $video_type = '';
        }

        return [
            'type' => $video_type,
            'id' => $video_id
        ];
    }
}
