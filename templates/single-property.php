<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>
<?php

use DPG\SingleAgent\Help;
use DPG\SingleAgent\Helpers\Property as Property;

the_post();

$property = new Property( get_the_ID() );
$address = $property->address();
$custom_fields = get_fields( get_the_ID() );

$items['property'] = $property;
$items['custom_fields'] = $custom_fields;
$items['status_tab'] = $property->statusTab();
$items['hide_contact'] = false;
$land_area = isset( $custom_fields['land_area'] ) ? $custom_fields['land_area'] : '';
$land_area_unit = isset( $custom_fields['land_area_unit'] ) ? $custom_fields['land_area_unit'] : 'squareMeter';
?>
<?php Help::getPartial( 'property.integrations.propps', array('property' => $property) ); ?>
<div class="dpg-wrapper single-property container-wrap">
  <header role="banner" class="slide-header">
	  <?php Help::getPartial( 'property.slider', $items ); ?>
  </header>
  <article>
    <header class="property-header">
      <div class="dpg-container">
        <address>
          <h2>
			  <?php
			  if ( isset( $items['custom_fields']['address_display'] ) && $items['custom_fields']['address_display'] === 'no' ) {
				  echo "{$address['suburb']}, {$address['postcode']}";
			  } else {
				  echo "{$address['street']}, {$address['suburb']}";
			  }
			  ?>
          </h2>
          <span class="fancy-line"></span>
        </address>
		  <?php Help::getPartial( 'property.details', [
			  'id'         => get_the_ID(),
			  'icon_class' => 'dark',
			  'property'   => $property,
		  ] ); ?>
      </div>
    </header>
    <div class="sub-header">
      <div class="dpg-container">
        <div class="wrapper">
						<span class="price">
				            <?php echo $property->displayPrice(); ?>
				        </span>
          <span class="status">
						<?php
			if ( $land_area ) {
				$land_suffix = 'sqm';
				switch ( $land_area_unit ) {
					case 'acre':
					case 'acres':
						$land_suffix = 'acres';
						break;
					case 'hectare':
					case 'hectares':
						$land_suffix = 'hectares';
						break;
					case 'squareMeter':
					case 'squareMeters':
						$land_suffix = 'sqm';
						break;
				}

				?>
              <span class="surface">
									<?php
				  echo $land_area . $land_suffix;
				  ?>
								</span>
			<?php } ?>
			  <?php echo $items['status_tab']; ?>
						</span>
        </div>
      </div>
    </div>
    <div class="dpg-container">
      <<?php Help::tab(); ?>-container id="property-content-tabs">
      <<?php Help::tab(); ?>-panel label="About">
		<?php if ( ! empty( $custom_fields['headline'] ) ): ?>
          <h2><?php echo $custom_fields['headline']; ?></h2>
          <span class="fancy-line fancy-line-dark"></span>
		<?php endif; ?>
      <div class="wp-content">
		  <?php the_content(); ?>
		  <?php if ( $property->status() === 'current' && ( $property->bond_amount() && $property->date_available() ) ) : ?>
            <div class="rental_info">
              <p>
                <span class="label">Available from:</span>
                <span class="date_available">
								                                <?php echo $property->date_available() ?>
								                            </span>
              </p>
              <p>
                <span class="label">Bond amount:</span>
                <span class="bond_amount">
								                                $<?php echo number_format( $property->bond_amount(),
						2 ); ?>
								                            </span>
              </p>
            </div>
		  <?php endif; ?>
      </div>
		<?php if ( $property->media()['property_media'] ) { ?>
          <a href="<?php echo $property->media()['property_media']; ?>" target="_blank"
              class="btn btn-block statement-of-information">Statement of Information</a>
		<?php } ?>
    </<?php Help::tab(); ?>-panel>
	  <?php if ( $property->media()['plan'] ) : ?>
    <<?php Help::tab(); ?>-panel label="Floor Plan">
    <img src="<?php echo $property->media()['plan']; ?>" alt="Floor Plan" style="max-width: 100%;">
  </<?php Help::tab(); ?>-panel>
	<?php endif; ?>
	<?php if ( $property->media()['3d_floor_plan_image'] ) : ?>
  <<?php Help::tab(); ?>-panel label="3D Floor Plan">
  <img src="<?php echo $property->media()['3d_floor_plan_image']; ?>" alt="3D Floor Plan" style="max-width: 100%;">
</<?php Help::tab(); ?>-panel>
<?php endif; ?>
<?php if ( $property->media()['video'] ) : ?>
  <<?php Help::tab(); ?>-panel label="Video">
	<?php $video_data = Help::getVideoData( $property->media()['video'] ); ?>
	<?php if ( in_array( $video_data['type'], [ 'youtube', 'vimeo' ] ) ) : ?>
		<?php
		$video = [
			'video_type' => $video_data['type'],
			'video_id'   => $video_data['id'],
		];
		Help::getPartial( 'video', [ 'video' => $video ] ); ?>
    <!--                    <div class="ui embed" data-source="--><?php //echo $video_type;?><!--"-->
    <!--                         data-id="--><?php //echo $video_id; ?><!--">-->
    <!--                    </div>-->
	<?php else: ?>
    <a href="<?php echo $video; ?>"
        class="btn"
        title="View Video"
        target="_blank"
    >
      View Video
    </a>
	<?php endif; ?>
  </<?php Help::tab(); ?>-panel>
<?php endif; ?>
<?php if ( $property->media()['3d_floor_plan'] ) : ?>
  <<?php Help::tab(); ?>-panel label="3D Tour">
	<?php Help::getPartial( '3d_floor_plan', [ 'url' => $property->media()['3d_floor_plan'] ] ); ?>
  </<?php Help::tab(); ?>-panel>
<?php endif; ?>
</<?php Help::tab(); ?>-container>
</div>
<?php
$hide_map = false;
if ( isset( $items['custom_fields']['address_display'] ) && $items['custom_fields']['address_display'] === 'no' ) {
	$hide_map = true;
}
if ( ! $hide_map ) {
	?>
  <google-map id="prop-map"
      lat="<?php echo $custom_fields['lat']; ?>"
      lon="<?php echo $custom_fields['lon']; ?>"
      assets-url="<?php echo plugins_url( 'dpg-wp-properties' ); ?>"
  ></google-map>
<?php } ?>
<?php if ( ! in_array( $custom_fields['property_status'], [ 'sold', 'leased' ] ) ) : ?>
  <section class="inspections-section">
    <div class="dpg-container">
      <h2>INSPECTION TIMES</h2>
		<?php Help::getPartial( 'property.inspections', $items ); ?>
    </div>
  </section>
<?php endif; ?>
<section class="agents-section">
  <div class="dpg-container">
    <h2>CONTACT AGENT</h2>
	  <?php Help::getPartial( 'property.agents', $items ); ?>
  </div>
</section>
<section class="tabs">
  <div class="dpg-container">
	  <?php Help::getPartial( 'property.resource-tabs', $items ); ?>
  </div>
</section>
<section class="neighbourhood">
  <div class="dpg-container">
	  <?php
	  $args = [
		  'post_type'      => 'neighbourhood',
		  'posts_per_page' => 1,
		  'title'          => get_field( 'address_suburb' )
	  ];
	  Help::getPartial( 'neighbourhood.discover', [ 'args' => $args ] );
	  ?>
  </div>
</section>
<?php
Help::getPartial( 'property.go-back', [] );
?>
</article>
</div>
<?php get_footer(); ?>
