<?php use DPG\SingleAgent\Help;

if (count($neighbourhood->discover())): ?>
    <div id="discover-content">
        <a name="lifestyle-anchor"></a>
        <?php foreach( $neighbourhood->discover() as $discover ) :  ?>
            <div class="group">
                <?php if ($discover['title']): ?>
                    <h3 class="entry-title text-center">
                        <?php echo $discover['title']; ?>
                    </h3>
                <?php endif; ?>
                <div class="discover-grid">
                    <?php foreach ($discover['discover_content_row'] as $row): ?>
                        <?php
                            $column_count = 'one';
                            switch ($row['layout']) {
                                case 'one_column':
                                    $column_count = 'one';
                                    break;
                                case 'two_columns':
                                    $column_count = 'two';
                                    break;
                                case 'two_columns_66_33':
                                    $column_count = 'two sixtysix-thirtythree';
                                    break;
                                case 'three_columns':
                                    $column_count = 'three';
                            };
                        ?>
                        <ul class="<?php echo $column_count ?? 'one'; ?> column row">
                            <?php foreach ($row as $column): ?>
                                <?php if (!in_array($column, ['one_column', 'two_columns', 'two_columns_66_33', 'three_columns']) && $column): ?>
                                    <li class="column">
                                        <?php $item = $column[0]; ?>

                                        <?php if ($item['acf_fc_layout'] == 'video'): ?>

                                            <div class="video">
	                                            <?php Help::getPartial( 'video', [ 'video' => $item ] ); ?>
                                            </div>
                                        <?php endif; ?>

                                        <?php if ($item['acf_fc_layout'] == 'image'): ?>
                                            <div class="image"
                                                 style="background-image:url('<?php echo $item['image']['sizes']['large']; ?>')">
                                                <?php if ($item['label']): ?>
                                                    <div class="label"><?php echo $item['label']; ?></div>
                                                <?php endif; ?>
                                            </div>

                                        <?php endif; ?>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
