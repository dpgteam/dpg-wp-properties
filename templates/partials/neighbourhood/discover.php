<?php

use DPG\SingleAgent\{
	Help, Icon, Helpers\Neighbourhood
};

$neighbourhoods = new WP_Query( $args );
?>
<?php if ( $neighbourhoods->have_posts() ): ?>
	<?php while( $neighbourhoods->have_posts() ): $neighbourhoods->the_post();
		$neighbourhood = new Neighbourhood( get_the_ID() );
		$image         = $neighbourhood->featured_image();
		?>
		<div id="discover" class="discover-tile">
			<a href="<?php the_permalink(); ?>">
				<?php if ( $image ) { ?>
					<bg-image prop-image="<?php Help::vueify($neighbourhood->featured_image());?>" :with-overlay="true"></bg-image>
				<?php } ?>
				<article class="content">
					<p>Discover</p>
					<h4 class="title">
						<?php the_title(); ?>
					</h4>
					<p class="postcode"><?php echo get_field( 'postcode' ); ?></p>
				</article>
			</a>
		</div>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
<?php endif; ?>
