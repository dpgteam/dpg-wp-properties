<?php

use DPG\SingleAgent\Help;

if ( ! isset( $neighbourhoods ) ) {
	$neighbourhoods = new WP_Query( [
		'post_type'      => 'neighbourhood',
		'posts_per_page' => - 1,
		'order'          => 'ASC',
		'orderby'        => 'title',
	] );
	$neighbourhoods = $neighbourhoods->posts;
}
?>
<?php
$hoods = [];
foreach ( $neighbourhoods as $key => $neighbourhood ) {
	$hoods[] = [
		'title'          => get_the_title( $neighbourhood->ID ),
		'image'          => get_field( 'image', $neighbourhood->ID ),
		'featured_image' => Help::makeFeaturedImageArray( $neighbourhood->ID ),
		'href'           => get_the_permalink( $neighbourhood->ID ),
	];
}
?>
<neighbourhood-grid prop-neighbourhoods="<?php Help::vueify( $hoods ); ?>">
</neighbourhood-grid>

