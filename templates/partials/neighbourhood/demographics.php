<?php
use DPG\API\Client;
use DPG\WP\Admin\Settings;
?>
<section class="demographics">
    <?php

    if ($demographics):
        $client = new Client();
        $response = $client->request('GET', 'suburb-median-prices', [
            'query' => [
                'api_token' => Settings::get_option( 'api_key', 'api' ),
                'where[0]' => 'state,' . get_field('state'),
                'where[1]' => 'suburb,' . get_the_title(),
                'where[2]' => 'property_type,house',
                'where[3]' => 'time_period,annual',
                'orderBy' => 'quarter_ending,desc',
                'limit' => 1
            ]
        ]);
        $median_house_price = @array_shift(json_decode($response->getBody()->getContents())->data);

        $response = $client->request('GET', 'suburb-median-prices', [
            'query' => [
                'api_token' => Settings::get_option( 'api_key', 'api' ),
                'where[0]' => 'state,' . get_field('state'),
                'where[1]' => 'suburb,' . get_the_title(),
                'where[2]' => 'property_type,unit',
                'where[3]' => 'time_period,annual',
                'orderBy' => 'quarter_ending,desc',
                'limit' => 1
            ]
        ]);
        $median_unit_price = @array_shift(json_decode($response->getBody()->getContents())->data);
        ?>
        <ul class="median-house-prices">
            <li>
                <header class="section-title">
                    <h3>Median House Price</h3>
                </header>
                <div class="content house">
                    <div class="header">
                        BUY<br>
                        <?php echo isset($median_house_price->median_price) ? '$' . number_format($median_house_price->median_price) : '-'; ?>
                    </div>
                </div>
            </li>
            <li>
                <header class="section-title">
                    <h3>Median Unit Price</h3>
                </header>
                <div class="content unit">
                    <div class="header">
                        BUY<br>
                        <?php echo isset($median_unit_price->median_price) ? '$' . number_format($median_unit_price->median_price) : '-'; ?>
                    </div>
                </div>
            </li>
        </ul>

    <?php endif; ?>
</section>
<a name="people-anchor" id="people-anchor"></a>
<section class="people">
    <header class="section-title">
        <h3>People of <?php the_title(); ?></h3>
    </header>
    <a name="people-anchor"></a>

    <?php if ($demographics): ?>
        <div class="demographics-container">
            <?php
                $top_three = [];
                if($demographics->population_under_15){
	                $top_three['Population under 15'] = $demographics->population_under_15;
	                $top_three['Population 15 to 65'] = $demographics->population_15_to_65;
	                $top_three['Population over 65']  = $demographics->population_over_65;
                } else {
	                $top_three['Population 0 to 19']  = $demographics->population_0_to_9 + $demographics->population_10_to_19;
	                $top_three['Population 20 to 60'] = $demographics->population_20_to_29 + $demographics->population_30_to_39 + $demographics->population_40_to_49 + $demographics->population_50_to_59;
	                $top_three['Population Over 60']  = $demographics->population_60_to_69 + $demographics->population_70_to_79 + $demographics->population_80_plus;
                }

                $detail = [];
                $detail['Population 0 to 9']   = $demographics->population_0_to_9;
                $detail['Population 10 to 19'] = $demographics->population_10_to_19;
                $detail['Population 20 to 29'] = $demographics->population_20_to_29;
                $detail['Population 30 to 39'] = $demographics->population_30_to_39;
                $detail['Population 40 to 49'] = $demographics->population_40_to_49;
                $detail['Population 50 to 59'] = $demographics->population_50_to_59;
                $detail['Population 60 to 69'] = $demographics->population_60_to_69;
                $detail['Population 70 to 79'] = $demographics->population_70_to_79;
                $detail['Population 80 plus']  = $demographics->population_80_plus;
            ?>
            <ul class="demographic-tabs">
                <li>
                    <a class="top-3 btn btn-block btn-default active">
                        Top 3
                    </a>
                </li>
                <li>
                    <a class="detail btn btn-block btn-default">
                        Details
                    </a>
                </li>
            </ul>

            <div class="demographic-content">
                <div>
                    <ul class="demographic top-3">
                        <?php foreach ($top_three as $key => $p): $percentage = round($p / $demographics->population_total * 100, 2);
                            $radius = 40;
                            $circumference = pi() * 2 * $radius;
                            $offset = ((100 - $percentage) / 100) * $circumference;
                            switch($key) {
                                case 'Population under 15':
                                case 'Population 0 to 19':
                                    $stroke = "#79bcff";
                                    break;
                                case 'Population 15 to 65' :
                                case 'Population 20 to 60' :
                                    $stroke = "#ff6666";
                                    break;
                                case 'Population over 65' :
                                case 'Population Over 60' :
                                    $stroke = "#68d09f";
                                    break;
                            }
                        ?>
                            <li class="item">
                                <div class="label">
                                    <?php echo $key; ?>
                                    <span class="percentage"><?php echo $percentage; ?>%</span>
                                </div>
                                <svg viewbox="0 0 100 100">
                                    <circle cx="50" cy="50" r="<?php echo $radius; ?>" fill="transparent"
                                            stroke-width="15"
                                            stroke="#EEE"/></circle>
                                    <circle cx="50" cy="50" r="<?php echo $radius; ?>" fill="transparent"
                                            stroke-width="15"
                                            stroke="<?php echo $stroke; ?>"
                                            stroke-dasharray="<?php echo $circumference; ?>"
                                            stroke-dashoffset="<?php echo $offset; ?>"/></circle>
                                </svg>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <ul class="demographic detail">
                        <?php
                        $max = round(100 / (max($detail) / $demographics->population_total * 100), 0);
                        foreach ($detail as $key => $p): $percentage = round($p / $demographics->population_total * 100, 2);
                            $color = "#aaa";
                            if ( in_array($key, ['Population 0 to 9', 'Population 10 to 19']) ) {
                                $color = "#79bcff";
                            } else  if( in_array($key, ['Population 20 to 29', 'Population 30 to 39', 'Population 40 to 49', 'Population 50 to 59']) ) {
                                $color = "#ff6666";
                            } else if( in_array($key, ['Population 60 to 69', 'Population 70 to 79', 'Population 80 plus']) ) {
                                $color = "#68d09f";
                            }
                        ?>
                            <li>
                                <div class="bar"
                                     style="width:<?php echo $percentage * ($max/2); ?>%; background:<?php echo $color; ?>">
                                     <span class="percentage"><?php echo $percentage; ?>%</span>
                                </div>
                                <span class="label"><?php echo $key; ?></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>
</section>

