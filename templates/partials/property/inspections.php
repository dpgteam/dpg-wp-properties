<?php

use DPG\SingleAgent\{
    Help
};
use DPG\SingleAgent\Helpers\{
    Property, Page
};
use DPG\WP\Admin\Settings;
use DPG\WP\Helpers\Icon;

$images = $property->images();
$image = $images[0]['url'] ?? '';
$id = get_the_ID();
/* Get the Office ID to find Agent Account. */
$office_id = get_posts(array(
    'numberposts' => 1,
    'post_type' => 'office',
    'fields' => 'ids',
    'meta_key' => 'branch_id',
    'meta_value' => get_field('branch_id', $id),
));

$office_id = $office_id[0] ?? 0;
if ($office_id) {
    $inspectAccount = get_field('inspect_account', $office_id);
} else {
    $inspectAccount = Settings::get_option('fallback_office_id', 'frontend');
}
?>
<?php if ($property->soldOrLeased() === false) : $inspection_times = $property->inspections(); ?>
	<section class="inspection-and-auction-times">
        <?php if (!empty($property->inspections()['inspection_times'])): ?>
            <?php
            $slice = min(count($property->inspections()['inspection_times']), 4);
            foreach (array_slice($property->inspections()['inspection_times'], 0, $slice) as $t): ?>
				<div class="inspection">
					<div class="content">
						<div class="center-helper">
							<i class="dpg-icon dpg-icon-clock"></i>
							<div class="day">
                                <?php echo date('l', $t['timestamp_start']); ?><br>
							</div>
							<div class="date">
                                <?php echo date('d M', $t['timestamp_start']); ?>
							</div>
						</div>
					</div>
                    <?php Help::getPartial('property.calendar-link', [
                        'label' => date('g:i', $t['timestamp_start']) . '-' . date('g:ia', $t['timestamp_end']),
                        'title' => 'Open for Inspection: ' . get_the_title(),
                        'location' => get_the_title(),
                        'start' => $t['timestamp_start'],
                        'end' => $t['timestamp_end'],
                    ]);

                    ?>

				</div>
            <?php endforeach;
            ?>

        <?php else: ?>
			<div class="inspection">
				<div class="content contact">
					<div class="center-helper">
						<strong>Contact Agent</strong>
					</div>
				</div>
			</div>
        <?php endif; ?>

        <?php if (!empty($property->inspections()['auction_date'])): ?>
            <?php $t = strtotime($property->inspections()['auction_date']); ?>
            <?php if ($t > time()) : ?>
				<div class="auction">
					<div class="content">
						<div class="center-helper">
							<i class="dpg-icon dpg-icon-ribbon"></i>
							<div class="type-title">
								AUCTION<br>
							</div>
							<div class="date">
                                <?php echo date('j M', strtotime($property->inspections()['auction_date'])); ?>
							</div>
						</div>
					</div>
                    <?php Help::getPartial('property.calendar-link', [
                        'label' => date('g:ia', strtotime($property->inspections()['auction_date'])),
                        'title' => 'Auction: ' . get_the_title(),
                        'location' => get_the_title(),
                        'start' => $t['timestamp_start'],
                        'end' => $t['timestamp_end'],
                    ]); ?>
				</div>
            <?php endif; ?>
        <?php endif; ?>
	</section>
    <?php
    //if (!empty($property->inspections()['inspection_times'])):
        if ($inspectAccount) {

            Help::getPartial('property.book-inspection-link', [
                'image' => urlencode($image),
                'property' => $property,
                'address' => $property->address(),
                'AgentAccountName' => $inspectAccount,
            ]);
        }
    //endif;
    ?>
<?php endif; ?>
