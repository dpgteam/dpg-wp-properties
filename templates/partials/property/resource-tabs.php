<?php

use DPG\SingleAgent\{
	Help, Helpers\Property
};
use DPG\WP\Admin\Settings;

extract( $items['custom_fields'] );
$is_sales = ! empty( $price );
$property = new Property;
?>
<tab-container id="single-property-resources">
	<tab-panel label="Area Map">
		<div class="about-area-map">
			<iframe id="area-map" frameborder="0"
			        src="https://www.google.com/maps/embed/v1/place?q=<?php echo $property->address()['suburb']; ?> <?php echo $property->address()['postcode']; ?>,Australia&key=<?php echo Settings::get_option( 'google_map_key', 'api' ); ?>"
			        allowfullscreen async defer
			></iframe>
			<div class="overlay"></div>
		</div>
	</tab-panel>
	<tab-panel label="Transport">
		<area-map assets-url="<?php echo plugins_url('dpg-wp-properties');?>" area-type="Transport"
		          lat="<?php echo $lat; ?>"
		          lon="<?php echo $lon; ?>"
		></area-map>
	</tab-panel>
	<tab-panel label="Schools">
		<area-map assets-url="<?php echo plugins_url('dpg-wp-properties');?>" area-type="Schools"
		          lat="<?php echo $lat; ?>"
		          lon="<?php echo $lon; ?>"
		></area-map>
	</tab-panel>
</tab-container>
<?php if ( true === false ) : ?>
	<?php
	/* Default Arguments for similar property listings. */
	$args = [
		'post_type'      => 'property',
		'posts_per_page' => - 1,
		'order'          => 'DESC',
		'orderby'        => 'post_modified',
		'meta_query'     => array(
			[
				'key'     => 'address_suburb',
				'value'   => $property->address()['suburb'],
				'compare' => 'LIKE'
			],
		)
	];
	/* Get current properties and add custom fields. */
	$args['meta_query'][1] = [
		'key'     => 'property_status',
		'value'   => 'current',
		'compare' => 'LIKE'
	];
	$currentProperties     = new WP_Query( $args );
	foreach ( $currentProperties->posts as $key => $prop ) {
		$currentProperties->posts[ $key ]->fields = get_fields( $prop->ID );
	}
	$args['posts_per_page'] = 9;
	$args['meta_query'][1]  = [
		'key'     => 'property_status',
		'value'   => 'sold',
		'compare' => 'LIKE'
	];
	$soldProperties         = new WP_Query( $args );
	foreach ( $soldProperties->posts as $key => $prop ) {
		$soldProperties->posts[ $key ]->fields = get_fields( $prop->ID );
	}
	$category = ( $is_sales ? 'sale' : 'lease' );
	?>
	<tab-panel label="Similar Properties">
		<property-grid prop-properties="<?php Help::vueify( $currentProperties->posts ); ?>"></property-grid>
	</tab-panel>
	<?php
	$label    = $is_sales ? 'Recently Sold' : 'Recently Leased';
	$category = $is_sales ? 'sold' : 'leased';
	?>
	<tab-panel label="<?php echo $label; ?>">
		<property-grid prop-properties="<?php Help::vueify( $soldProperties->posts ); ?>"></property-grid>
	</tab-panel>
<?php endif; ?>
