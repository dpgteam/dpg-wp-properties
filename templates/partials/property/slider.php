<?php
/**
* This partial generates a slideshow for a property post.
 * Include it inside a property post loop and it will
 * get the images.
 */
use DPG\SingleAgent\Helpers\Property;
$property = new Property(get_the_ID());
?>
<?php if ( $property->images() ): ?>
    <div class="slider-wrapper">
        <div class="slide-loading-screen">
            <div class="load-spinner"></div>
        </div>

        <div class="slideshow" style="overflow: hidden;">
            <div class="slides">
                <?php foreach ($property->images() as $key => $image): ?>
                    <div class="slide">
                        <div class="image-wrapper">
                            <img <?php echo $key > 3 ? 'data-lazy="' . $image['url'].'"' : 'src="' . $image['url'].'"'  ?>>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="slideshow">
        <div style="height:200px;"></div>
    </div>
<?php endif; ?>
