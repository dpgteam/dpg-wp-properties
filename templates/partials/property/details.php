<?php

use DPG\WP\Helpers\Icon;

$modifier = isset( $modifier ) ? $modifier : '';
?>
<div class="details">
	<?php if ( ! empty( $property->type() ) && ! $property->commercial() ): ?>
		<div class="property icons">
			<?php if ( ! empty( $property->features()['bedrooms'] ) ): ?>
				<span>
                    <span class="nr"><?php echo $property->features()['bedrooms']; ?></span>
					<i class="dpg-icon dpg-icon-bed"></i>
                </span>
			<?php endif; ?>
			<?php if ( ! empty( $property->features()['bathrooms'] ) ): ?>
				<span>
                    <span class="nr"><?php echo $property->features()['bathrooms']; ?></span>
					<i class="dpg-icon dpg-icon-bath"></i>
                </span>
			<?php endif; ?>

			<?php if ( ! empty( $property->features()['car_spaces'] ) ): ?>
				<span>
                    <span class="nr"><?php echo $property->features()['car_spaces']; ?></span>
					<i class="dpg-icon dpg-icon-car"></i>
                </span>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</div>
