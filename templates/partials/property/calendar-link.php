<div class="addtocalendar atc-style-menu-wb <?php echo strtolower($label); ?>-calendar">
    <a class="atcb-link btn btn-block" title="Add to Calendar" style="position: relative; ">
        <?php echo $label; ?>
    </a>
    <var class="atc_event" style="display: none;">
        <var class="atc_date_start"><?php echo date('Y-m-d H:i:s', $start); ?></var>
        <var class="atc_date_end"><?php echo date('Y-m-d H:i:s', $end); ?></var>
        <var class="atc_timezone">Australia/Melbourne</var>
        <var class="atc_title"><?php echo $title; ?></var>
        <var class="atc_description"></var>
        <var class="atc_location"><?php echo $location; ?></var>
        <var class="atc_privacy">private</var>
    </var>
</div>
