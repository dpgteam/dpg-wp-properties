<?php

use DPG\WP\Admin\Settings;

$props_api_key = Settings::get_option('propps_api_key', 'api');

$id =  strtoupper($property->slug());
if($props_api_key){
?>
<script>
(function(w,d,P,e,o,x,s) {
    w[o]=w[o]||{exec:function(){var a=Array.from(arguments);return new P(function(rs,rj){w[o].q.push({rs,rj,a})})},q:[]};
    s=d.createElement(e);s.defer=1;s.src=x;d.head.appendChild(s)
})(window, document, Promise, 'script', 'OfferKit', 'https://script.propps.com/v1')

OfferKit.exec('init', { appId: '<?php echo $props_api_key; ?>' });
OfferKit.exec('mount', {
    listingId: '<?php echo $id; ?>',
})
</script>
<?php
}
?>
