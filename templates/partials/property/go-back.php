<?php
$url = $_SERVER['HTTP_REFERER'];
if ($url) {
    ?>
	<section class="go-back-properties">
		<div class="dpg-container text-center">
			<a href="<?php echo $url ?>" class="btn btn-fill">Back to results</a>
		</div>
	</section>
<?php } ?>