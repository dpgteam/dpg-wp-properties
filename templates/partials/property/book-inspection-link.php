<?php
$query = [
    'AgentID' => $AgentAccountName,
    'address' => $address['street'] . $address['suburb'],
    'imgURL' => $image,
    'uniqueId' => $property->unique_id()
];
$type = $property->type() == 'rental' ? 'rental' : 'sale';
if (($type == 'sale' || $type == 'rental') && $property->status() === 'current') {
    $query['type'] = $type;
}
$url = 'https://book.inspectrealestate.com.au/Register?' . http_build_query($query);
?>
<div class="text-center">
	<a target="_blank"
	   href="<?php echo $url; ?>"
	   class="btn btn-fill">Book an inspection</a>
</div>