<?php

use DPG\SingleAgent\{
    Help, Icon
};
use DPG\SingleAgent\Helpers\Page;
use DPG\SingleAgent\Helpers\Agent;

/**
 * This partial outputs a list of agents
 * attached to the property being viewed.
 */
$hide_contact = isset($hide_contact) ? $hide_contact : false;
?>

<?php if ($agents = !empty($custom_fields['property_agent']) ? $custom_fields['property_agent'] : null): ?>

	<section class="property-agents">
        <?php
        $items = [];
        foreach ($agents as $key => $agent) {
            $agent = new Agent($agent->ID);
            $items[$key] = [
                'agent' => get_post($agent->id()),
                'mobile' => $agent->mobile(),
                'photo' => $agent->image(),
                'featured_image' => $agent->featured_image(),
                'title' => get_field('title', $agent->id()),
                'email' => get_field('email', $agent->id()),
                'href' => get_the_permalink($agent->id()),
            ];
        }
        ?>
		<agent-grid prop-agents="<?php Help::vueify($items);?>" property-title="<?php the_title(); ?>"
		            :show-email-button="<?php Help::vueify(!$hide_contact); ?>"></agent-grid>
	</section>
<?php endif; ?>
