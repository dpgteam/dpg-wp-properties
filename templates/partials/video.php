<div class="video embed-responsive embed-responsive-16by9">
    <?php if ('vimeo' === strtolower($video['video_type'])) : ?>
        <iframe class="embed-responsive-item" src="//player.vimeo.com/video/<?php echo $video['video_id']; ?>?api=false&amp;autoplay=false&amp;byline=false&amp;color=%23444444&amp;portrait=false&amp;title=false" width="16" height="9" frameborder="0" scrolling="no" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe>
    <?php endif; ?>
    <?php if ('youtube' ===  strtolower($video['video_type'])) : ?>
        <iframe class="embed-responsive-item" src="//www.youtube.com/embed/<?php echo $video['video_id']; ?>?autoplay=false&autohide=true&modestbranding=true&jsapi=false" width="16" height="9" frameborder="0" scrolling="no" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen="true"></iframe>
    <?php endif; ?>
</div>
