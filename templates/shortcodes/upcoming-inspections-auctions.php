<?php

use DPG\SingleAgent\Helpers\Property;
use DPG\WP\Admin\Settings;

$args              = [
    'post_type'      => 'property',
    'posts_per_page' => 2000,
    'order'          => 'DESC',
    'orderby'        => 'post_modified',
    //    'date_query' => array(
    //        array(
    //            'after' => '6 months ago',
    //            'column' => 'post_modified',
    //        ),
    //    ),
    'meta_query'     => array(
        'relation' => 'AND',
        array(
            'key'     => 'property_status',
            'value'   => 'withdrawn',
            'compare' => '!=',
        )
    )
];
$properties        = new WP_Query($args);
$final_auctions    = [];
$final_inspections = [];
foreach ($properties->posts as $key => $post) {
    $property = new Property($post);
    if ($property->soldOrLeased()) {
        continue;
    }
    $inspection_times = $property->inspections();
    if ( ! $inspection_times['auction_date'] && empty($inspection_times['inspection_times'])) {
        continue;
    }
    $post->fields = get_fields($post->ID);
    $post->status = $property->determinePropertyStatus();
    $post->type = $property->determinePropertyType();
    foreach ($post->fields['property_agent'] as $i => $agent) {
        $post->fields['property_agent'][$i]->fields = [
            'email' => get_field('email', $agent->ID),
            'phone' => get_field('phone', $agent->ID),
            'mobile' => get_field('mobile', $agent->ID),
        ];
    }

    if ( ! empty($inspection_times['inspection_times'])) {
        foreach ($inspection_times['inspection_times'] as $inspection_time) {
            $inspection = [
                'time'     => $inspection_time['timestamp_start'],
                'date'     => $inspection_time['formatted'],
                'property' => $post
            ];
            $timestamp  = $inspection['time'];
            $day        = date('Y-m-d', $timestamp);
            if ( ! isset($final_inspections[$day])) {
                $final_inspections[$day] = [];
            }
            if ( ! isset($final_inspections[$day][$timestamp])) {
                $final_inspections[$day][$timestamp] = [];
            }
            $final_inspections[$day][$timestamp][] = $inspection;
        }
    }

    if ($inspection_times['auction_date']) {
        $auction   = [
            'date'     => $inspection_times['auction_date'],
            'property' => $post
        ];
        $timestamp = strtotime($auction['date']);
        $day       = date('Y-m-d', $timestamp);
        if ( ! isset($final_auctions[$day])) {
            $final_auctions[$day] = [];
        }
        if ( ! isset($final_auctions[$day][$timestamp])) {
            $final_auctions[$day][$timestamp] = [];
        }
        $final_auctions[$day][$timestamp][] = $auction;
    }
}

if ($final_auctions) {
	ksort($final_auctions);
    foreach ($final_auctions as $key => $final_auction) {
        ksort($final_auction);
        $final_auctions[$key] = $final_auction;
    }
}
if ($final_inspections) {
	ksort($final_inspections);
    foreach ($final_inspections as $key => $final_inspection) {
        ksort($final_inspection);
        $final_inspections[$key] = $final_inspection;
    }
}

?>
	<div class="dpg-wrapper dpg-properties-wrapper dpg-properties-events-wrapper">
		<property-events
				:prop-timezone-offset="<?php echo get_option('gmt_offset'); ?>"
				prop-inspections="<?php echo htmlspecialchars(json_encode($final_inspections)) ?>"
				prop-auctions="<?php echo htmlspecialchars(json_encode($final_auctions)) ?>">
		</property-events>
	</div>
<?php wp_reset_postdata(); ?>