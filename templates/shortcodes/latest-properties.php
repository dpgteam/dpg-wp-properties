<?php

use DPG\SingleAgent\Helpers\Property;
use DPG\WP\Admin\Settings;

$meta_queries = array(
	'residential'      => array(
		'current',
		'sold'
	),
	'commercial_sale'  => array(
		'current',
		'sold'
	),
	'commercial_lease' => array(
		'current',
		'leased'
	),
	'commercial'       => array(
		'current',
		'sold'
	),
	'commercialland'   => array(
		'current',
		'leased'
	),
	'land'             => array(
		'current',
		'sold'
	),
	'rental'           => array(
		'current',
		'leased'
	),
	'rural'            => array(
		'current',
		'sold'
	)
);

$args = [
	'post_type'      => 'property',
	'posts_per_page' => 3,
	'order'          => 'DESC',
	'orderby'        => 'post_modified',
	'date_query'     => array(
		array(
			'after'  => '6 months ago',
			'column' => 'post_modified',
		),
	),
];
$posts = array();
foreach ( $meta_queries as $type => $statuses ) {
	foreach ( $statuses as $status ) {
		$args['meta_query'] = array(
			'relation' => 'AND',
			array(
				'key'     => 'property_type',
				'value'   => $type,
				'compare' => '=',
			),
			array(
				'key'     => 'property_status',
				'value'   => $status,
				'compare' => '=',
			),
		);

		$properties = new WP_Query( $args );
		$properties->posts = array_merge( $properties->posts, $posts );
		$properties->post_count = count( $properties->posts );
		$posts = $properties->posts;
	}
}

foreach ( $properties->posts as $key => $post ) {
	$property = new Property( $post );
	$properties->posts[ $key ]->fields = get_fields( $post->ID );
	$properties->posts[ $key ]->status = $property->determinePropertyStatus();
	$properties->posts[ $key ]->type = $property->determinePropertyType();
	foreach ( $properties->posts[ $key ]->fields['property_agent'] as $i => $agent ) {
		$properties->posts[ $key ]->fields['property_agent'][ $i ]->fields = [
			'email'  => get_field( 'email', $agent->ID ),
			'phone'  => get_field( 'phone', $agent->ID ),
			'mobile' => get_field( 'mobile', $agent->ID ),
		];
	}
}

$preffered_order = Settings::get_option( 'property_preffered_order', 'frontend' );
if ( ! $preffered_order ) {
	$preffered_order = 'DATE-DESC';
}


?>
  <div class="dpg-wrapper dpg-properties-wrapper">
    <property-listings
        prop-properties="<?php echo htmlspecialchars( json_encode( $properties->posts ) ) ?>"
        :only-latest="true"
        prop-default-order="<?php echo( $preffered_order ); ?>"
    >
    </property-listings>
  </div>
<?php wp_reset_postdata(); ?>