<?php

use DPG\SingleAgent\Help;

?>
<div class="dpg-wrapper dpg-neighbourhoods-wrapper">
	<div class="neighbourhoods-section">
		<div class="dpg-container">
<!--			<i class="dpg-icon dpg-icon-map-marker"></i>-->
<!--			<h3>GET TO KNOW YOUR AREA BETTER</h3>-->
			<?php Help::getPartial( 'neighbourhood.list' ); ?>
		</div>
	</div>
</div>

