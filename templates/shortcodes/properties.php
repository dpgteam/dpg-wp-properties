<?php

use DPG\SingleAgent\Helpers\Property;
use DPG\WP\Admin\Settings;

$args = [
    'post_type' => 'property',
    'posts_per_page' => 2000,
    'order' => 'DESC',
    'orderby' => 'post_modified',
    'date_query' => array(
        array(
            'after' => '12 months ago',
            'column' => 'post_modified',
        ),
    ),
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'property_status',
            'value' => 'withdrawn',
            'compare' => '!=',
        )
    )
];
$properties = new WP_Query($args);
foreach ($properties->posts as $key => $post) {
    $property = new Property($post);
    $properties->posts[$key]->fields = get_fields($post->ID);
    $properties->posts[$key]->status = $property->determinePropertyStatus();
    $properties->posts[$key]->type = $property->determinePropertyType();
    foreach ($properties->posts[$key]->fields['property_agent'] as $i => $agent) {
        $properties->posts[$key]->fields['property_agent'][$i]->fields = [
            'email' => get_field('email', $agent->ID),
            'phone' => get_field('phone', $agent->ID),
            'mobile' => get_field('mobile', $agent->ID),
        ];
    }
}
$preffered_view = Settings::get_option('property_preffered_view', 'frontend');
if (!$preffered_view) {
    $preffered_view = 'true';
}
$preffered_order = Settings::get_option('property_preffered_order', 'frontend');
if (!$preffered_order) {
    $preffered_order = 'DATE-DESC';
}
$filter_status = isset($_GET['filter-status']) && $_GET['filter-status'] ? $_GET['filter-status'] : null;
?>
	<div class="dpg-wrapper dpg-properties-wrapper">
		<property-listings
				prop-properties="<?php echo htmlspecialchars(json_encode($properties->posts)) ?>"
				prop-show-grid="<?php echo($preffered_view); ?>"
				prop-default-order="<?php echo($preffered_order); ?>"
				assets-url="<?php echo plugins_url('dpg-wp-properties'); ?>"
                <?php if ($filter_status) { ?>prop-filter-status="<?php echo $filter_status; ?>"<?php }; ?>>
		</property-listings>
	</div>
<?php wp_reset_postdata(); ?>
