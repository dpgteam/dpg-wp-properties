<?php
    use DPG\WP\Admin\Settings;
?>
<!-- The dpgwrapper class is used to bring vue to the div tag. -->
<!-- Settings::get_option('email_config', 'pricefinder') brings the email from the admin panel. -->
<!-- The email is configured in the left sidebar -> DPG Properties -> Settings -> Pricefinder -->
<!-- You should config that email in order to receive emails from the platform. -->
<!-- You should also config the pricefinder api key in the same place. -->
<div class="dpg-wrapper">
    <forms
        email-prop="<?php echo Settings::get_option('email_config', 'pricefinder'); ?>"
        api-key-prop="<?php echo Settings::get_option('pricefinder_api_key', 'pricefinder'); ?>"
    >
    </forms>
</div>
<?php wp_reset_postdata(); ?>
