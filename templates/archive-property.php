<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();
echo do_shortcode( '[dpg-properties]' );
get_footer();



