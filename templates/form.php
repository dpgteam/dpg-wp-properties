<?php
    /**
     * Template Name: Form
     */
    get_header();

    use DPG\WP\Admin\Settings;
?>
<!-- The dpgwrapper class is used to bring vue to the div tag. -->
<!-- Settings::get_option('email_config', 'config') brings the email from the admin panel. -->
<!-- The email is configured in the left sidebar -> DPG Properties -> Settings -> Config -->
<!-- You should config that email in order to receive emails from the platform. -->
<div class="dpg-wrapper">
    <forms email-prop="<?php echo Settings::get_option('email_config', 'config'); ?>"></forms>
</div>
<?php get_footer(); ?>
