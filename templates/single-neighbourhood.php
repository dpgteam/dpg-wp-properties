<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header(); ?>
<?php

use DPG\API\Client;
use DPG\SingleAgent\Help;
use DPG\SingleAgent\Helpers\Neighbourhood;
use DPG\WP\Admin\Settings;

$neighbourhood = new Neighbourhood(get_the_ID());
$suburb = get_the_title();
// Get demographics data
$client = new Client();
try {
    $response = $client->request('GET', 'suburb-demographics', [
        'query' => [
            'api_token' => Settings::get_option('api_key', 'api'),
            'where[0]' => 'state,' . get_field('state'),
            'where[1]' => 'suburb,' . get_the_title(),
        ]
    ]);
    $demographics = @array_shift(json_decode($response->getBody()->getContents())->data);
} catch (Exception $exception) {
    // do nothing
}
?>
	<div class="dpg-wrapper container-wrap">
        <?php while (have_posts()) : the_post(); ?>
			<header role="banner" class="neighbourhood">
                <?php if ($neighbourhood->featured_image()) { ?>
					<bg-image prop-image="<?php Help::vueify($neighbourhood->featured_image()); ?>"
					          :with-overlay="true"></bg-image>
                <?php } ?>
				<section class="content">
					<span>Discover</span>
					<h3><?php the_title(); ?></h3>
					<address>
                        <?php echo $neighbourhood->state(); ?> <?php echo $neighbourhood->postcode(); ?>
					</address>
				</section>
			</header>
			<article class="neighbourhood">
				<div class="dpg-container">
					<header class="section-title">
						<h2>Welcome to <?php the_title(); ?></h2>
					</header>
					<div class="wp-content">
                        <?php if ($content = get_the_content()): ?>
                            <?php the_content(); ?>
                        <?php endif; ?>
					</div>
					<neighbourhood-intro-tabs></neighbourhood-intro-tabs>
				</div>
			</article>
			<div class="neighbourhood-map" style="margin-bottom: 1rem;">
				<div class="overlay"></div>
				<iframe class="neighbourhood map" frameborder="0"
				        style="max-height: 60vh; min-width: 100vw; overflow: hidden;"
				        src="https://www.google.com/maps/embed/v1/place?q=<?php the_title(); ?>,<?php echo get_field('state'); ?>, <?php echo get_field('postcode'); ?>,Australia&key=<?php echo Settings::get_option('google_map_key', 'api'); ?>"
				        async defer></iframe>
			</div>
			<a name="lifestyle-anchor" id="lifestyle-anchor"></a>
            <?php if ($social_eazie_widget_id = get_field('social_eazie_widget_id')): ?>
				<section class="neighbourhood-social dpg-container">
					<header class="section-title">
						<h2 class="text-center">What’s happening in your local area</h2>
					</header>
					<div id="social-eazie-widget-area" data-widget="<?php echo $social_eazie_widget_id; ?>"></div>
				</section>
            <?php endif; ?>
			<div class="dpg-container">
				<a name="homes-anchor" id="homes-anchor"></a>
                <?php
                $args = [
                    'post_type' => 'property',
                    'posts_per_page' => 3,
                    'meta_query' => [
                        ['key' => 'property_type', 'value' => 'rental', 'compare' => '!='],
                        ['key' => 'property_status', 'value' => 'current'],
                        ['key' => 'address_suburb', 'value' => get_the_title()]
                    ],
                    'meta_key' => 'created_at',
                    'orderby' => 'meta_num',
                    'order' => 'desc'
                ];
                $latest_sales_listing = new WP_Query($args);
                foreach ($latest_sales_listing->posts as $key => $post) {
                    $latest_sales_listing->posts[$key]->fields = get_fields($post->ID);
                }
                ?>
				<section class="properties">
                    <?php if ($latest_sales_listing->have_posts()): ?>
						<header class="section-title">
							<h3>Latest <?php echo $suburb; ?> Sales Listings</h3>
						</header>
						<property-grid
								prop-properties="<?php Help::vueify($latest_sales_listing->posts); ?>"></property-grid>
                    <?php endif; ?>
                    <?php wp_reset_postdata(); ?>

                    <?php
                    $args = [
                        'post_type' => 'property',
                        'posts_per_page' => 3,
                        'meta_query' => [
                            ['key' => 'property_type', 'value' => 'rental'],
                            ['key' => 'property_status', 'value' => 'current'],
                            ['key' => 'address_suburb', 'value' => get_the_title()]
                        ],
                        'meta_key' => 'created_at',
                        'orderby' => 'meta_num',
                        'order' => 'desc'
                    ];
                    $latest_rental_property = new WP_Query($args);
                    foreach ($latest_rental_property->posts as $key => $post) {
                        $latest_rental_property->posts[$key]->fields = get_fields($post->ID);
                    }
                    ?>
                    <?php if ($latest_rental_property->have_posts()): ?>
						<header class="section-title">
							<h3>Latest <?php echo $suburb; ?> Rental Listings</h3>
						</header>
						<property-grid
								prop-properties="<?php Help::vueify($latest_rental_property->posts); ?>"></property-grid>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
				</section>
                <?php
                if (isset($demographics)) {
                    Help::getPartial('neighbourhood.demographics', ['demographics' => $demographics]);
                }
                ?>
                <?php Help::getPartial('neighbourhood.discover-content', ['neighbourhood' => $neighbourhood]); ?>
			</div>
        <?php endwhile; ?>
	</div>
<?php get_footer(); ?>