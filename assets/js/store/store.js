import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// root state object.
// each Vuex instance is just a single state tree.
const defaultState = {
  state :{
    test: 'Hello State!',
    /**
     * Stores content for tabbed panels.
     * @type {Array}
     */
    tabContainers: {
      default: {
        activePanel: null,
        tabs: [],
      }
    }
  },
  mutations : {
  },
  actions : {
  },
  getters : {
  },
};


// import themeStore from './themeStore';
// import themeOptionsStore from './themeOptionsStore';

// A Vuex instance is created by combining the state, mutations, actions,
// and getters.
export default new Vuex.Store({
  modules: {
    defaultState,
  }
});
