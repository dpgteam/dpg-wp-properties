window.$ = window.jQuery;
import Importer from './admin/importer';


$(document).ready(function () {
	let serverData = window.dpg_wp_properties;

	new Importer(serverData.apiEndpoint);
});
