export default {
    data() {
        return {
            resizeTimer  : null,
            windowWidth  : window.innerWidth,
            windowHeight : window.innerHeight,
            elementWidth : 'auto',
            elementHeight: 'auto',
            mainNavHeight: 0,
            isMobile     : true,
        };
    },
    computed: {
        elementSquareStyle() {
            return  {
                width    : (this.elementWidth) + 'px',
                height   : (this.elementWidth) + 'px',
                maxWidth    : (this.elementWidth) + 'px',
                maxHeight   : (this.elementWidth) + 'px',
                minWidth    : (this.elementWidth) + 'px',
                minHeight   : (this.elementWidth) + 'px',
                boxSizing: 'border-box',
            };
        },
    },
    methods: {
        /**
         * Returns background image style declaration for {property}.
         * @param  {Object} property
         * @return {Object}
         */
        propertyBackgroundStyle(property) {
            return {
                backgroundImage   : `url('${property.fields.images[0].url}')`,
                // backgroundSize    : 'cover',
                // backgroundPosition: 'center center',
                // boxSizing         : 'border-box',
                // height            : this.elementWidth + 'px',
                // width             : this.elementWidth + 'px',
                // maxWidth          : '100%',
            };
        },
        updateViewport() {
            clearTimeout(this.resizeTimer);
            let update = () => {
                this.windowWidth   = window.innerWidth;
                this.windowHeight  = window.innerHeight;
                this.isMobile      = this.windowWidth <= 440 ? true : false;
                // this.mainNavHeight = document.querySelector('nav[role=navigation]').offsetHeight;

                let el = $(this.$el);
                let parent = el.parent().parent(),
                    width  = Math.ceil(parent.width()),
                    cols   = 3,
                    offset = 15;
                if ( this.windowWidth <= 600 ) {
                    cols = 1;
                    offset = 14;
                } else if ( this.windowWidth <=  920 ) {
                    cols =  2;
                }
                this.elementWidth  = Math.floor(width / cols) - offset;
                this.elementHeight = Math.floor(width / cols) - offset;
            };
            this.resizeTimer = setTimeout(update, 75);
        },
    },

    watch: {
    },

    mounted() {
        this.updateViewport();
        window.addEventListener('resize', this.updateViewport);
    }

};
