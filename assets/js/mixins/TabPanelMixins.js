export default {
    data() {
        return {
        };
    },
    computed: {
    },
    methods: {
        addAppraisalForm() {
            console.log('Add Appraisal');
            let form = $('#appraisal-form');
            if ( this.activePanel === 'Request an Appraisal' ) {
              console.log('Is Appraisal Tab');
                setTimeout(() => {
                    let appraisalTab = $('#Request-an-Appraisal-tab, .tab-request-an-appraisal');
                    let found = appraisalTab.find('#appraisal-form').length;
                    if( appraisalTab.length )
                      form.appendTo(appraisalTab).show();
                        // $('#appraisal-form').clone().css('display', 'block').appendTo(appraisalTab);
                }, 50);
            } else {
              if ( form.length )
                form.hide().appendTo('body');
            }
        },
        addContactForm() {
            console.log('Add Contact');
            let form = $('#agent-contact-form');
            if ( this.activePanel === 'Get In Touch' ) {
              console.log('Is Contact Tab');
                setTimeout(() => {
                    let contactTab = $('#Get-In-Touch-tab > div, .tab-get-in-touch');
                    if( contactTab.length )
                      form.appendTo(contactTab).show();
                        // $('#agent-contact-form').clone().css('display', 'block').appendTo(contactTab);
                }, 50);
            } else {
              if ( form.length )
                form.hide().appendTo('body');
            }
        },
        addSocialWidget() {
          let iframe = $('#social-eazie-wall-iframe');
          if ( this.activePanel === "What's On" ) {
              setTimeout(() => {
                  let contactTab = $('#Whats-On-tab #social-eazie-widget-area, .tab-whats-on');
                  if( contactTab.length ) {
                    if ( iframe.length )
                      iframe.appendTo(contactTab).show();
                  }
              }, 50);
          } else {
              if ( iframe.length )
                  iframe.hide().appendTo('body');
          }
        },
        addTrustPilotWidget() {
          let inlineWidget = $('.trustpilot-widget-wrapper-inline');
          if ( this.activePanel === "Testimonials" ) {
            setTimeout(() => {
              let trustpilot = $('#Testimonials-tab .trustpilot-widget-wrapper');
              if( trustpilot.length ) {
                if ( inlineWidget.length )
                  inlineWidget.appendTo(trustpilot).show();
              }
            }, 50);
          } else {
            if ( inlineWidget.length )
              inlineWidget.hide().appendTo('body');
          }
        }
    },
    watch: {
    },
    mounted() {
    }
};
