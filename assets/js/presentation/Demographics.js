let $ = jQuery;
export default {
  init() {
    // this.buttons = $('.demographic-tabs');
    this.content = $('.demographic-content');
    this.detailContent.hide();
    this.tabButtonEvents();
  },
  get buttons() {
    return $('.demographic-tabs .btn');
  },
  get topButton() {
    return $('.demographic-tabs .top-3');
  },
  get detailButton() {
    return $('.demographic-tabs .detail');
  },
  get topContent() {
    return $('.demographic-content .top-3');
  },
  get detailContent() {
    return $('.demographic-content .detail');
  },
  /**
   * Adds click events to toggle class and content views.
   * @return {void}
   */
  tabButtonEvents() {
    let self = this;
    this.buttons.on('click', (event) => {
      if ( ! $(event.target).hasClass('active') )
        self.buttons.toggleClass('active');
    });
    this.topButton.on('click', (event) => {
      self.topContent.fadeIn(50);
      self.detailContent.hide();
    });
    this.detailButton.on('click', (item) => {
      self.topContent.hide();
      self.detailContent.fadeIn(50);
    });
  }
};
