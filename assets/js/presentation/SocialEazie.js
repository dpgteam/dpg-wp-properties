let $ = jQuery;
export default {
  loadInterval: null,
  init() {
    this.appendSocialEazieWidget();
    this.appendSocialEazieWidetToHomePage();
  },
  appendSocialEazieWidetToHomePage() {
    if ( window.location.pathname === '/' ) {
    }
  },
  /**
   * If a widget container is found, it gets its widgetId
   * attribute, loads the widget script and appends the
   * widget iframe to the widget container.
   * @return {void}
   */
  appendSocialEazieWidget() {
    let widget = $('#social-eazie-widget-area');
    if ( widget.length ) {
      let widgetId = widget.data('widget') ? widget.data('widget') : 426;
      this.loadWidgetScript(widgetId);
      let appendWidget = () => {
          let iframe = $('#social-eazie-wall-iframe');
          if ( iframe.length ) {
              iframe.clone().appendTo(widget);
          } else {
              setTimeout(appendWidget, 25);
          }
      };
      appendWidget();
    }
  },
  /**
   * Loads Social Easie widget JS with {widgetId}.
   * @param  {Number} widgetId
   * @return {void}
   */
  loadWidgetScript(widgetId = null ) {
    if ( widgetId ) {
      $('#widget-area').append(`<script id="social-eazie-widget" src="//widget.app.socialeazie.com/wall/widget/${widgetId}"></script>`);
    }
  }
};

