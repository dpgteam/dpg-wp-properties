export default {
	init() {
		this.setupPropertySlider();
		// this.setupPropertySummarySlider();
	},
	/**
	 * If {selector} is found, an interval is set and a
	 * new Slick carousel is initalised with {options}.
	 * @param  {string} selector jQuery selector
	 * @param  {Object} options  Slick Options
	 * @return {void}
	 */
	setupSlider(selector, options) {
		selector = $(selector);
		if (!selector.length) return;

		// mimic load event, as the slick plugin doesn't provide one
		selector.on('init', () => {
			// Start timer
			let timer = setInterval(() => {
				if (!selector.hasClass('slick-loading')) {
					$('.slide-loading-screen').remove();
					clearInterval(timer);
					timer = false;
				}
			}, 50);
		});
		selector.slick(options);
	},
	/**
	 * If viewing a property page, initialise the image slideshow.
	 * @return {void}
	 */
	setupPropertySlider() {
		if (window.location.pathname.includes('/property/') || $('#request-an-appraisal').length > 0) {
			this.setupSlider('.slideshow .slides', {
				centerMode: true,
				centerPadding: '0',
				dots: false,
				variableWidth: true,
				slidesToShow: 1,
				infinite: true,
				arrows: true,
				pauseOnHover: false,
				prevArrow: '<i class="fa fa-angle-left slick-prev slick-arrow"></i>',
				nextArrow: '<i class="fa fa-angle-right slick-next slick-arrow"></i>',
				autoplay: false,
				lazyLoad: 'progressive',
				responsive: [
					{
						breakpoint: 800,
						centerMode: false,
						variableWidth: false,
						arrows: true,
						dots: true,
						settings: {
							centerPadding: 0
						}
					}
				]
			});
		}
	},
	setupPropertySummarySlider() {
		if (window.location.pathname.includes('/properties/')) {

		}
	}
};

