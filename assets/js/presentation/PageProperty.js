export default {
	/**
	 * Checks to see if this is the property page and calls page methods.
	 * @return {void}
	 */
	init() {
		if ( window.location.pathname.indexOf('/property/') === 0 ) {
			this.loadAtcScript();
			// this.loadAgentContactForm();
		}
	},
	/**
	 * Checks to see if AddToCalender JS script is loaded and if not
	 * appends it to the document.
	 * @return {void}
	 */
	loadAtcScript() {
		/* Add to Calendar Link */
		if (window.addtocalendar) {
			if (typeof window.addtocalendar.start == "function") return;
		}
		if (window.ifaddtocalendar === undefined) {
			window.ifaddtocalendar = 1;
			let script = document.createElement('script');
			script.type = 'text/javascript';
			script.charset = 'UTF-8';
			script.async = true;
			script.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://addtocalendar.com/atc/1.5/atc.min.js';
			document.getElementsByTagName('body')[0].appendChild(script);
		}
	},
	/**
	 * Adds the Agent contact form and dynamically links it to
	 * the agent whose email button was clicked.
	 * @return {void}
	 */
	loadAgentContactForm() {
		var emailBtn    = $('.email-agent-btn'),
			contactForm = $('.agent-contact-form'),
			closeBtn    = contactForm.find('.fa-times'),
			addressData = emailBtn.data('address');

		$('.property-address input').val(addressData);

		emailBtn.on('click', function() {
			var $this   = $(this);
			$('.agent-title').text($this.data('name'));
			$('.agent-contact-field input').val($this.data('email'));
			contactForm.slideDown(225);
		});
		closeBtn.on('click', function(){
			contactForm.slideUp(225);
		});
	},
};
