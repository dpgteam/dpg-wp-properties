import Vimeo from '@vimeo/player';
import $ from 'jquery';
window.Vimeo = Vimeo;

export default {
  _content : null,
  _header  : null,
  _playBtn : null,
  onReady(){
  },
  init() {
    if(!this.supportVHUnits()){
      this.setFullWindowHeight();
    }
    this.initializeVideo();
    // this.setupHeaderVimeoPlayer();
    // this.setupHeaderLocalPlayer();
  },
  get header() {
    return this.setGetJQueryObject('_header', 'header[role=banner]');
  },
  get content() {
    return this.setGetJQueryObject('_content', 'header[role=banner] > .content, header[role=banner] > .overlay');
  },
  get playBtn() {
    return this.setGetJQueryObject('_playBtn', '#play-video');
  },
  supportVHUnits(){
    let elemWidth = parseInt(getComputedStyle(document.querySelector("#check-vw"), null).width, 10);
    let halfWidth = parseInt(window.innerWidth / 2, 10);
    return elemWidth === halfWidth;
  },
  setFullWindowHeight(){
    this.header.height(window.innerHeight);
  },
  initializeVideo() {
    let localVideo = document.getElementById('header-video-local');
    if ( ! localVideo ) return;

    // we add this event, as this is loaded
    this.header.find('.video .video-thumbnail').addClass('video-loaded');

    this.playBtn.modalVideo({
      channel: this.playBtn.data('channel'),
      openCallback: () => {
        localVideo.pause();
        this.content.fadeOut(50);
      },
      closeCallback: () => {
        localVideo.play();
        this.content.fadeIn(50);
      }
    });
  },
  // /**
  //  * Adds click event to playBtn to restart local video and unmute audio.
  //  * @return {void}
  //  */
  // setupHeaderLocalPlayer() {
  //   let video = document.getElementById('header-video-local');
  //   if ( ! video ) return;
  //   $(video).attr('height', $('header[role=banner]').height());
  //   this.playBtn.on('click', () => {
  //       video.muted       = false;
  //       video.currentTime = 0;
  //       video.volume      = 0.5;
  //   });
  //   console.log('local video');
  // },
  // /**
  //  * Adds click even to playBtn to restart Vimeo video and umute audio.
  //  * @return {void}
  //  */
  // setupHeaderVimeoPlayer() {
  //   this.resizeHeaderVimeoPlayer();
  //   let iframe = document.querySelector('#header-video-vimeo');
  //   if ( ! iframe ) return;
  //
  //   let player  = new Vimeo(iframe);
  //   player.setVolume(0);
  //   player.play();
  //   this.playBtn.on('click', () => {
  //       player.getVolume().then( (volume) => {
  //           player.setVolume( volume > 0 ? 0 : 0.5 );
  //           if ( volume === 0 ) player.setCurrentTime(0);
  //       });
  //   });
  //
  // },
  // /**
  //  * Resizes the header vimeo player to fill its container.
  //  * @return {void}
  //  */
  // resizeHeaderVimeoPlayer() {
  //     let timer  = null,
  //         iframe = $('header[role=banner] .video > iframe');
  //     let resize = () => {
  //         iframe.width(window.innerWidth / 2);
  //         iframe.attr('width', window.innerWidth / 2);
  //         iframe.attr('height', 1 * $('header[role=banner]').height() / 2);
  //     };
  //     resize();
  //     $(window).resize(function() {
  //         clearTimeout(timer);
  //         timer = setTimeout(resize, 100);
  //     });
  // },
  /**
   * If not set, queries DOM for jQuery {selector} and
   * stores it to {key}, then returns.
   * @param {String} key
   * @param {String} selector
   * @return {jQuery}
   */
  setGetJQueryObject(key, selector) {
    if ( ! this[key] ) {
      this[key] = $(selector);
    }
    return this[key];
  },
};
