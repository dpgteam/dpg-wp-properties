export default class Importer {
    constructor(endpoint) {
        this.endpoint = endpoint;

        this.$ = {};
        this.$.importPropertiesButton = $('.button-primary.import-properties');
        this.$.importPropertiesResetButton = $('.button-link.reset-properties-date');

        this.$.importOfficesButton = $('.button-primary.import-offices');
        this.$.importOfficesResetButton = $('.button-link.reset-offices-date');

        this.$.resetPropertiesButton = $('.button-primary.reset-all-property-data');
        this.bindUI();

        this.synced = 0;
    }

    bindUI() {
        let self = this;

        // import properties
        this.$.importPropertiesButton.on('click', function (e) {
            e.preventDefault();
            let $btn = $(this);
            let $spinner = $(this).closest('.action').find('.spinner');
            self.sync($btn, $spinner);
        });
        this.$.importPropertiesResetButton.on('click', function (e) {
            e.preventDefault();
            self.syncReset();
        });

        // import offices
        this.$.importOfficesButton.on('click', function (e) {
            e.preventDefault();
            let $btn = $(this);
            let $spinner = $(this).closest('.action').find('.spinner');
            self.syncOffices($btn, $spinner);
        });

        this.$.importOfficesResetButton.on('click', function (e) {
            e.preventDefault();
            self.syncOfficesReset();
        });

        // full reset properties
        this.$.resetPropertiesButton.on('click', function (e) {
            e.preventDefault();
            let $btn = $(this);
            let $spinner = $(this).closest('.action').find('.spinner');
            self.fullReset($btn, $spinner);
        });
    }

    sync($btn, $spinner) {
        $.ajax(this.endpoint + '/sync', {
            type: 'GET',
            dataType: 'json',
            beforeSend: Importer.startAjaxUI($btn, $spinner)
        }).done(response => {
            if (response.success) {
                // if it's not up to date, we call sync again
                // we use an ajax approach, to avoid timeouts
                if (!response['up_to_date']) {
                    this.sync($btn, $spinner);
                } else {
                    confirm('All properties are up to date!');
                    Importer.stopAjaxUI($btn, $spinner);
                }
            } else {
                alert(response.message);
                Importer.stopAjaxUI($btn, $spinner);
            }
        }).fail(err => {
            Importer.stopAjaxUI($btn, $spinner);
            Importer.unknownError(err);
        });

    }

    syncReset() {
        $.ajax(this.endpoint + '/sync/reset', {
            type: 'GET',
            dataType: 'json'
        }).done(() => {
            confirm('Properties updated date has been reset.')
        });
    }

    syncOffices($btn, $spinner) {
        $.ajax(this.endpoint + '/sync-offices', {
            type: 'GET',
            dataType: 'json',
            beforeSend: Importer.startAjaxUI($btn, $spinner)
        }).done(response => {
            if (response.success) {
                // if it's not up to date, we call sync again
                // we use an ajax approach, to avoid timeouts
                if (!response['up_to_date']) {
                    this.syncOffices($btn, $spinner);
                } else {
                    confirm('All offices are up to date!');
                    Importer.stopAjaxUI($btn, $spinner);
                }
            } else {
                alert(response.message);
                Importer.stopAjaxUI($btn, $spinner);
            }
        }).fail(err => {
            Importer.stopAjaxUI($btn, $spinner);
            Importer.unknownError(err)
        });
    }

    syncOfficesReset() {
        $.ajax(this.endpoint + '/sync-offices/reset', {
            type: 'GET',
            dataType: 'json'
        }).done(() => {
            confirm('Offices updated date has been reset.')
        });
    }

    fullReset($btn, $spinner) {
        $.ajax(this.endpoint + '/full-reset', {
            type: 'GET',
            dataType: 'json',
            beforeSend: Importer.startAjaxUI($btn, $spinner)
        }).done(response => {
            if (response.success) {
                // if it's not up to date, we call sync again
                // we use an ajax approach, to avoid timeouts
                if (!response['all_done']) {
                    this.fullReset($btn, $spinner);
                } else {
                    confirm('All data has been removed!');
                    Importer.stopAjaxUI($btn, $spinner);
                }
            } else {
                alert(response.message);
                Importer.stopAjaxUI($btn, $spinner);
            }
        }).fail(err => {
            Importer.stopAjaxUI($btn, $spinner);
            Importer.unknownError(err);
        });

    }


    /**
     *
     * @param $btn
     * @param $spinner
     */
    static startAjaxUI($btn, $spinner) {
        if (!$spinner.hasClass('is-active')) {
            $spinner.addClass('is-active');
        }
        if (!$btn.hasClass('disabled')) {
            $btn.addClass('disabled');
        }
    }

    /**
     *
     * @param $btn
     * @param $spinner
     */
    static stopAjaxUI($btn, $spinner) {
        $btn.removeClass('disabled');
        $spinner.removeClass('is-active');
    }

    static unknownError(err, $btn, $spinner) {
        console.log(err);
        Importer.stopAjaxUI($btn, $spinner);
        if (confirm('Unknown error')) {
            window.location.reload();
        }
    }
}