<template>
    <section class="tab-panel"
        :id="id"
        v-if="activePanel"
        :aria-labelledby="id + '-label'"
        :aria-hidden=" ! activePanel"
        role="tabpanel"
    >
        <div>
            <slot></slot>
        </div>
    </section>
</template>

<script>

import uiMixins from './uiMixins.js';
import StateMixins from '../../mixins/StateMixins.js';
export default {

    props: {
        /**
         * A display label which is also used to
         * identify this tab panel.
         * @type {Object}
         */
        label: {
            type: String,
            required: true
        },

    },

    data() {
        return {
            ready: false,
            /**
             * Indicates whether this is the currently
             * visible tab panel.
             * @type {Boolean}
             */
        };
    },
    computed: {
        slug() {
            return this.label.split(' ').join('-').replace('&amp;', '').replace(/&/g, '').replace("'", '');
        },
        id() {
            return this.slug + '-tab';
        },
        /**
         * Indicates that this tab panel is currently being viewed.
         * @return {Boolean}
         */
        activePanel() {
            return this.label === this.$parent.activePanel;
        },
        tabClass() {
            let className = 'tab-' + this.label;
            className = className.toLowerCase();
            className = className.split(' ').join('-');

            return className;
        },
    },
    mixins: [
        StateMixins
    ],
    methods: {
      /**
       * Event handler for 'panelWasChanged' events. Compares
       * activated tab label to self and sets activePanel to
       * true on match.
       * @return {void}
       */
      setActivePanel(label) {
      },
    },
    /**
     * Broadcasts 'panelWasCreated' event with this
     * panel's label so that it is added to the
     * tab container.
     * @return {void}
     */
    mounted() {
        this.ready = true;
    },
};
</script>
