/* jshint undef: false */

import $ from 'jquery';

export default {

   methods: {

      /**
       * $(document).ready() wrapper.
       * @param  {function} func    Function to call on ready.
       * @return {void}
       */
      $(func) {

         $(document).ready(()=>{
            func.call();
         });

      },

      /**
       * Sets a collection of elements to have equal
       * width and height.
       * @param  {jQuery Selector} $items
       * @param  {Integer} index  $items index used to compute calculate
       *                          square dimensions against.
       * @return {void}
       */
      squares($items, index = 2) {

         this.$( ()=>{
            $items = $($items);
            $items.height(
               $items.eq(index).width()
            );
         });

      },

      /**
       * Sets a collection of elements to have equal width relative
       * to their parent container and the number of item rows.
       * @param  {jQuery Selector} $items
       * @param  {Integer} rows
       * @return {void}
       */
      eqWidth($items, rows = 1) {

         this.$( ()=>{
            $items = $($items);

            $items.width(
               ($items.parent().width() / $items.length) / rows
            );
         });

      },

   },

};
