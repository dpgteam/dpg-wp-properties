window.$ = window.jQuery;

window.slugify = text => {
    return text.toString()
        .toLowerCase()
        .replace(/\s+/g, '-')
        .replace(/[^\w\-]+/g, '')
        .replace(/\-\-+/g, '-')
        .replace(/^-+/, '')
        .replace(/-+$/, '');
};

require('slick-carousel');

import PageProperty from './presentation/PageProperty.js';
import Demographics from './presentation/Demographics.js';
import SocialEazie from './presentation/SocialEazie.js';
import SlickCarousel from './presentation/SlickCarousel.js';

import Vue from 'vue';

import store from './store/store';
import Vuex from 'vuex';
import VueScrollTo from 'vue-scrollto';

Vue.config.devtools = true;
Vue.config.debug = true;
Vue.use(VueScrollTo, {
    container: 'body',
    duration: 500,
    easing: 'ease',
    offset: -50,
    onDone: false,
    onCancel: false
});

Vue.use(Vuex);


import AgentBlock from './components/agent/AgentBlock.vue';
import AgentGrid from './components/agent/AgentGrid.vue';
import AreaMap from './components/AreaMap.vue';
import GoogleMap from './components/GoogleMap.vue';
import PropertyGrid from './components/property/PropertyGrid.vue';
import PropertyListings from './components/PropertyListings.vue';
import PropertyEvents from "./components/PropertyEvents.vue";
import TabContainer from './components/ui/TabContainer.vue';
import TabPanel from './components/ui/TabPanel.vue';
import AccordionContainer from './components/ui/AccordionContainer.vue';
import AccordionPanel from './components/ui/AccordionPanel.vue';
import BgImage from './components/ui/BgImage.vue';
import NeighbourhoodGrid from './components/neighbourhood/NeighbourhoodGrid.vue';
import NeighbourhoodIntroTabs from './components/neighbourhood/NeighbourhoodIntroTabs.vue';
import Forms from './components/pricefinder-comps/Forms.vue'


$(document).on('ready', function () {
    if (!$('.dpg-wrapper').length) {
        return;
    }

    /**
     * Vue Application
     */
    const app = new Vue({
        el: '.dpg-wrapper',
        store,
        components: {
            BgImage,
            TabContainer,
            TabPanel,
            AccordionContainer,
            AccordionPanel,
            GoogleMap,

            AgentBlock,
            AgentGrid,
            AreaMap,

            PropertyGrid,
            PropertyListings,
            PropertyEvents,

            NeighbourhoodGrid,
            NeighbourhoodIntroTabs,

            Forms
        },
        data() {
            return {};
        },
        mounted() {
        }
    });


    SlickCarousel.init();
});
$(window).on('load', function () {
    SocialEazie.init();
    PageProperty.init();
    Demographics.init();
});
