<div class="wrap dvsi-wrap">
	<div id="icon-options-general" class="icon32"></div>
	<h1>DPG Properties</h1>
	<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-2">
			<div id="post-body-content">
				<div class="meta-box-sortables ui-sortable">
					<br class="clear">
					<div class="postbox" style="overflow: hidden">
						<h2><span>Properties</span></h2>
						<div class="inside">
							<p>Imports or updates all properties from the DPG API that have been created or updated
								since the last time this was run (on first run, it starts with the first property). The
								API key and Agency ID are needed before starting this process. If the Agent ID field is
								filled, only the properties for that agent will get imported.</p>
							<p>Clicking reset will reset the last updated date and that will cause the import process to
								start over and *update* all of the properties.</p>
							<div class="action" style="float: right; margin-bottom: 15px;">
								<button type="button" class="button-link reset-properties-date"
								        style="color: #bc0b0b; padding-right: 10px;">
									Reset last updated date
								</button>
								<div class="spinner"></div>
								<a class="button-primary import-properties" href="#">Import properties</a>
							</div>
						</div>
					</div>
					<div class="postbox" style="overflow: hidden">
						<h2><span>Offices</span></h2>
						<div class="inside">
							<p>Imports all offices from the DPG API that have been updated or created since last time
								this was run (on first run, it starts with the first office). The API key and Agency ID
								are needed before starting this process.</p>
							<p>Clicking reset will reset the last updated date and that will cause the import process to
								start over and *update* all of the offices.</p>
							<div class="action" style="float: right; margin-bottom: 15px;">
								<button type="button" class="button-link reset-offices-date"
								        style="color: #bc0b0b; padding-right: 10px;">
									Reset last updated date
								</button>
								<div class="spinner"></div>
								<a class="button-primary import-offices" href="#">Import offices</a>
							</div>
						</div>
					</div>
                    <?php if (isset($_GET['dev'])) { ?>
						<div class="postbox" style="overflow: hidden">
							<h2><span>Full Property reset</span></h2>
							<div class="inside">
								<p>Remove all properties, agents offices and neighbourhoods, and reset the last updated
									date.</p>
								<div class="action" style="float: right; margin-bottom: 15px;">
									<div class="spinner"></div>
									<a class="button-primary reset-all-property-data" href="">Remove property data</a>
								</div>
							</div>
						</div>
                    <?php } ?>
				</div>
			</div>
		</div>
		<br class="clear">
	</div>
</div>