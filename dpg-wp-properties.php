<?php
/**
 * Plugin Name: DPG WP Properties
 * Description: Handles importing and displaying properties using the DPG API.
 * Version: 0.5.4
 * @package DPG
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Define DPG_PLUGIN_FILE.
if ( ! defined( 'DPG_PLUGIN_FILE' ) ) {
	define( 'DPG_PLUGIN_FILE', __FILE__ );
}

include_once dirname( __FILE__ ) . '/vendor/autoload.php';

/**
 * Route for sending email using wordpress function wp_mail.
 * The basic wp_mail function is overidden by a plugin used to send emails via smtp.
 */
function send_email(WP_REST_Request $request) {
	wp_mail($request['email'], 'You have a new lead', $request['message'], array('Content-Type: text/html; charset=UTF-8'));
}

/**
 * This is how the email route is being registered to wordpress.
 */
add_action('rest_api_init', function() {
	register_rest_route('dpg/v1/', 'mail', [
		'methods' => 'POST',
		'callback' => 'send_email'
	]);
});

function run() {
	return \DPG\WP\Plugin::instance();
}

run();
