const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.setResourceRoot('../');
mix.setPublicPath('public');

mix
	.sass('assets/scss/main.scss', 'css')
	.js('assets/js/main.js', 'js')
	.js('assets/js/admin.js', 'js')
	.browserSync({
		proxy: 'http://dpg-wp-agent.local/'
	})
	.sourceMaps()
	.options({
		processCssUrls: false
	})
	.webpackConfig({
		externals: {
			jquery: 'jQuery'
		}
	})
	.disableSuccessNotifications();
